<?php

namespace App\Http\Resources;

use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $image = collect();
        if ($this->getMedia('photo-product')) {
            foreach ($this->getMedia('photo-product') as $media) {
                $image[] = new ImageResource($media);
            }
        }
        $vendorImage = collect();
        foreach ($this->vendor->getMedia('vendor-logo') as $media) {
            $vendorImage[] = new ImageResource($media);
        }
        $product = Product::where('vendor_id', $this->vendor->id)->count();
        $order = OrderDetail::whereHas('order', function ($query) {
            $query->where('vendor_id', $this->vendor->id);
        })
            ->where('order_status_id', 4)
            ->count();
        return [
            'id' => (string)$this->id,
            'attributes' => [
                'name' => $this->name,
                'slug' => $this->slug,
                'description' => $this->description,
                'price' => $this->price,
                'status' => $this->status,
                'image' => $image,
                'rating' => $this->rating,
                'duration' => $this->duration,
                'revision' => $this->revision,
                'need_shipping' => $this->need_shipping,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ],
            'relevant' => [
                'category' => [
                    'attributes' => [
                        'id' => $this->category->id,
                        'name' => $this->category->name,
                    ],
                ],
                'vendor' => [
                    'atributes' => [
                        'slug' => $this->vendor->slug,
                        'logo' => $vendorImage,
                        'name' => $this->vendor->business_name,
                        'description' => $this->vendor->description,
                        'join' => $this->vendor->created_at,
                        'location' => $this->vendor->city,
                        'rating' => 4,
                        'total_product' => $product,
                        'total_order' => $order,
                    ],
                ],
            ],
        ];
    }
}
