<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SearchGoogleMapsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $results = $this->resource['results']; // Mengambil semua hasil dari respons pencarian

        $formattedData = [];

        foreach ($results as $result) {
            $formattedData[] = [
                'address' => $result['formatted_address'],
                'location' => [
                    'latitude' => $result['geometry']['location']['lat'],
                    'longitude' => $result['geometry']['location']['lng'],
                ],
                'name' => $result['name'],
            ];
        }

        return $formattedData;
    }
}
