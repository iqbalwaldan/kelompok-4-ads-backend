<?php

namespace App\Http\Resources;

use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\User;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $orderDetail = OrderDetail::where('order_id', $this->id)->first();

        $product = Product::withTrashed()->find($orderDetail->product_id);

        if ($product === null || $product->trashed()) {
            $photoProduct = null;
            $productDescription = 'Product not available';
            $productPrice = 0;
        } else {
            $photoProduct = collect();
            if ($product->getMedia('photo-product')) {
                foreach ($product->getMedia('photo-product') as $media) {
                    $photoProduct[] = new ImageResource($media);
                }
            }

            if ($photoProduct->isEmpty()) {
                $photoProduct = [];
            } else {
                $photoProduct = $photoProduct[0];
            }

            $productDescription = $product->description;
            $productPrice = $product->price;
        }

        $vendor = Vendor::find($this->vendor_id);
        $orderer = User::find($this->user_id);
        $dueDate = Carbon::parse($this->order_date)->addDay();
        $need_shipping = true;
        if ($product->need_shipping == 0){
            $need_shipping = false;
        }

        return [
            'project_info' => [
                'id' => $this->id,
                'transaction_code' => $this->transaction_code,
                'title' => $orderDetail->title,
                'brief' => $orderDetail->brief,
                'reason_reject' => $orderDetail->reason_reject,
                'order_date' => $this->order_date,
                'due_date' => $dueDate->format('Y-m-d H:i:s'),
                'attachment' => $orderDetail->link,
                'product_info' => [
                    'image' => $photoProduct,
                    'name' => $product->name,
                    'description' => $productDescription,
                    'price' => $productPrice,
                    'revision' => $orderDetail->revision,
                    'need_shipping' => $need_shipping,
                    'status' => config('data.order_status')[$orderDetail->order_status_id]['label'],
                ],
                'vendor_info' => [
                    'name' => $vendor->business_name,
                ],
                'user_info' => [
                    'name' => $orderer->first_name,
                ],
            ],
        ];
    }
}
