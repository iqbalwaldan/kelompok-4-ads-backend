<?php

namespace App\Http\Resources;

use App\Models\Link;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class VendorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $logo = collect();
        if ($this->getMedia('vendor-logo')) {
            foreach ($this->getMedia('vendor-logo') as $media) {
                $logo[] = new ImageResource($media);
            }
        }

        $banner = collect();
        if ($this->getMedia('vendor-banner')) {
            foreach ($this->getMedia('vendor-banner') as $media) {
                $banner = new ImageResource($media);
            }
        }

        $ktp = collect();
        if ($this->getMedia('vendor-ktp')) {
            foreach ($this->getMedia('vendor-ktp') as $media) {
                $ktp[] = new ImageResource($media);
            }
        }

        // $porto = collect();
        // if ($this->getMedia('vendor-portofolio')) {
        //     foreach ($this->getMedia('vendor-portofolio') as $media) {
        //         $porto[] = new ImageResource($media);
        //     }
        // }
        $link = Link::where('vendor_id', $this->id)->get();
        return [
            'id' => (string)$this->id,
            'attributes' => [
                'data_diri' => [
                    'name' => $this->name,
                    'dob' => $this->dob,
                    'nik' => $this->nik,
                    'email' => $this->email,
                    'phone_number' => $this->phone_number,
                    'ktp' => $ktp,
                ],
                'data_bisnis' => [
                    'business_name' => $this->business_name,
                    'slug' => $this->slug,
                    'logo' => $logo,
                    'banner' => $banner,
                    'description' => $this->description,
                    'email_business' => $this->email_business,
                    'phone_business' => $this->phone_business,
                    'address' => $this->address,
                    'province' => $this->province,
                    'city' => $this->city,
                    'postal_code' => $this->postal_code,
                    'status' => $this->status,
                    'latitude' => (float)$this->latitude,
                    'longitude' => (float)$this->longitude,
                    'instagram' => new LinkResource($link->where('name', 'instagram')->first()),
                    'website' => new LinkResource($link->where('name', 'website')->first()),
                ],
                'upload_portofolio' => [
                    'portofolio' => new LinkResource($link->where('name', 'portofolio')->first()),
                ],
                'informasi_tambahan' => [
                    'know' => $this->know,
                    'reason' => $this->reason,
                ],
            ]
        ];
    }
}
