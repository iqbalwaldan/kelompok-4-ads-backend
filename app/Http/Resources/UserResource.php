<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {

        $photoProfile = collect();
        if ($this->getMedia('user-photo-profile')) {
            foreach ($this->getMedia('user-photo-profile') as $media) {
                $photoProfile[] = new ImageResource($media);
            }
        }
        return [
            'id' => (string)$this->id,
            'attributes' => [
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'photo_profile' => $photoProfile,
                'email' => $this->email,
                'address' => $this->address,
                'phone_number' => $this->phone_number,
                'gender' => $this->gender,
                'isAdmin' => $this->isAdmin,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at
            ]
        ];
    }
}
