<?php

namespace App\Http\Resources;

use App\Models\User;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user = User::find($this->user_id);
        $photoProfile = collect();
        if ($user->getMedia('user-photo-profile')) {
            foreach ($user->getMedia('user-photo-profile') as $media) {
                $photoProfile[] = new ImageResource($media);
            }
        }
        $vendor = Vendor::find($this->vendor_id);
        $logoVendor = collect();
        if ($vendor->getMedia('vendor-logo')) {
            foreach ($vendor->getMedia('vendor-logo') as $media) {
                $logoVendor = new ImageResource($media);
            }
        }

        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'user_id' => $this->user_id,
            'vendor_id' => $this->vendor_id,
            'revision' => $this->revision,
            'message' => $this->message,
            'sender' => $this->sender,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user' => [
                'name' => $user->first_name . ' ' . $user->last_name,
                'logo' => $photoProfile,
            ],
            'vendor' => [
                'name' => $vendor->business_name,
                'slug' => $vendor->slug,
                'logo' => $logoVendor,
            ],
        ];
    }
}
