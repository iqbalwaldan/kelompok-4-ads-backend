<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserListOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $photoProduct = collect();
        foreach ($this->product->getMedia('photo-product') as $media) {
            $photoProduct[] = new ImageResource($media);
        }
        if ($photoProduct->isEmpty()) {
            $photoProduct = [];
        } else {
            $photoProduct = $photoProduct[0];
        }
        return [
            'id' => $this->id,
            'image' => $photoProduct,
            'title' => $this->title,
            'brief' => $this->brief,
            'vendor' => $this->order->vendor->business_name,
            'price' => $this->order->total_payment,
            'date' => $this->order->order_date,
            'status' => config('data.order_status')[$this->order_status_id]['label'],
        ];
    }
}
