<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class RedirectIfNotVendor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {   
        if (Auth::check()) {
            $user = Auth::user();
            
            if ($user->getTable() == 'vendors') {
                return $next($request);
            } else {
                return response()->json([
                    'data' => null,
                    'message' => 'You are not authorized to make this request',
                    'status'=> '403'
                ], 403);
            }
        }
    }
}
