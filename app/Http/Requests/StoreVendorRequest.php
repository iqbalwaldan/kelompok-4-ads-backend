<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class StoreVendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            // Data diri
            'name' => ['required','string','max:255'],
            'dob' => ['required','date'],
            'nik' => ['required','string','max:255','unique:vendors,nik'],
            'email' => ['required','string','max:255','unique:vendors,email'],
            'phone_number' => ['required','string','max:255','unique:vendors,phone_number'],
            'password' => ['required', Password::defaults()],
                // Photo
            'ktp' => ['required','image','mimes:jpeg,png,jpg','max:2048'],
            // Data Bisnis
            'business_name'=>['required','string','max:255'],
            'slug'=>['required','string','max:255','unique:vendors,slug'],
                // Photo
            'logo' => ['required','image','mimes:jpeg,png,jpg','max:2048'],
            'email_business' => ['required','string','max:255','unique:vendors,email_business'],
            'phone_business' => ['required','string','max:255','unique:vendors,phone_number'],
            'address'=>['required','string','max:255'],
            'province'=>['required','string','max:255'],
            'city'=>['required','string','max:255'],
            'postal_code'=>['required','string','max:255'],
            'latitude'=>['required','max:255'],
            'longitude'=>['required','max:255'],
            'instagram' => ['required','string','max:255'],
            'website' => ['string','max:255'],
            // Portofolio
                // File
            'portofolio' => ['required','file','mimes:pdf','max:2048'],
            // Informasi Tambahan
            'know' => ['required','string','max:255'],
            'reason' => ['required','string','max:255'],
        ];
    }
}
