<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            // 'vendor_id' => 'required|exists:vendors,id',
            'category_id' => ['required','exists:categories,id'],
            'name' => ['required','string'],
            'description' => ['required','string'],
            'price' => ['required','integer'],
            // 'slug' => ['required','string','unique:products,slug'],
            'photo-product.*' => ['required','image','mimes:jpeg,png,jpg,gif,svg','max:2048'],
        ];
    }
}
