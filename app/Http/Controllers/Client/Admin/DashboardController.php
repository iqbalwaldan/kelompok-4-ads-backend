<?php

namespace App\Http\Controllers\Client\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Payment;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDO;
use Yajra\DataTables\Facades\DataTables;

class DashboardController extends Controller
{
    public function index()
    {
        $totalUser = User::where('isAdmin', 0)->count();
        $totalPemesanan = Payment::where('status', 'settlement')->sum('amount');
        $totalPemesanan = "Rp" . number_format($totalPemesanan, 2, ',', '.');
        // $rate = Product::get()->avg('rating');

        return view('admin/pages/dashboard/index', [
            'admin' => auth()->user(),
            'totalUser' => $totalUser,
            'totalPemesanan' => $totalPemesanan,
            'rate' => 0
        ]);
    }

    public function getTopUsers()
    {
        $topUsers = DB::table('payments')
            ->join('users', 'users.id', '=', 'payments.user_id')
            ->select('payments.user_id', 'users.first_name', DB::raw('SUM(payments.amount) AS total_amount'))
            ->where('payments.status', '=', 'settlement') // Menambahkan kondisi WHERE
            ->groupBy('payments.user_id', 'users.first_name')
            ->orderByDesc('total_amount')
            ->take(5)
            ->get();

        return response()->json(['topUsers' => $topUsers]);
    }

    public function getTransactionProduct()
    {
        $results = DB::table('categories')
            ->select('categories.name', DB::raw('COALESCE(SUM(order_details.subtotal), 0) AS subtotal'))
            ->leftJoin('products', 'categories.id', '=', 'products.category_id')
            ->leftJoin('order_details', 'products.id', '=', 'order_details.product_id')
            ->groupBy('categories.name')
            ->get();
        return response()->json($results);
    }

    // public function index(Request $request)
    // {
    //     $shipments = Shipment::with(['sender', 'recipient'])->orderBy('created_at', 'desc')->take(5)->get();
    //     $active = Shipment::active()->get()->count();
    //     $delivered = Shipment::delivered()->get()->count();
    //     $courier = Courier::accepted()->get()->count();
    //     $user = User::role('User')->get()->count();
    //     return view('admin.dashboard', [
    //         'shipments' => $shipments,
    //         'active'    => $active,
    //         'delivered' => $delivered,
    //         'courier'   => $courier,
    //         'user'      => $user
    //     ]);
    // }
}
