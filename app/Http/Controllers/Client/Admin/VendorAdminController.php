<?php

namespace App\Http\Controllers\Client\Admin;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class VendorAdminController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $users = Vendor::query();
            return DataTables::of($users)
                ->addIndexColumn()
                ->addColumn('action', function ($item) {
                    $button = '<div class="dropdown">
                                <button class="btn btn-secondary custom dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Action
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="/vendor/show/' . $item->slug . '">Show</a></li>
                                </ul>
                            </div> ';
                    return $button;
                })
                ->make();
        }
        return view('admin/pages/vendor/index',[
            'admin' => auth()->user(),
        ]);
    }
    public function show(Vendor $vendor)
    {

        return view('admin/pages/vendor/show', [
            "vendor" => $vendor,
            'admin' => auth()->user(),
        ]);
    }
    public function update(Request $request, Vendor $vendor)
    {
        // Validasi data yang diterima dari permintaan
        $request->validate([
            'status' => 'required|in:verified,process', // Sesuaikan dengan pilihan status yang ada
        ]);
        // Perbarui status vendor
        $vendor->status = $request->input('status');
        $vendor->save();

        // Redirect kembali ke halaman yang sesuai atau tampilkan pesan sukses
        return redirect('/vendor')->with('success', 'Status vendor diperbarui.');
    }
}
