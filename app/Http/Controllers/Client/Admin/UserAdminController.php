<?php

namespace App\Http\Controllers\Client\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UserAdminController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $users = User::query();
            return DataTables::of($users)
                ->addIndexColumn()
                ->addColumn('action', function ($item) {
                    $button = '<div class="dropdown">
                                <button class="btn btn-secondary custom dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Action
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="/user/delete/' . $item->id . '">Delete</a></li>
                                </ul>
                            </div> ';
                    return $button;
                })
                ->make();
        }
        return view('admin/pages/user/index',[
            'admin' => auth()->user(),
        ]);
    }
    public function delete(string $id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/user')->with('success', 'User berhasil dihapus.');
    }
}
