<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends ApiController
{
    public function index()
    {
        try {
            $categories = Category::all();
            return $this->respondSuccess('Success', CategoryResource::collection($categories));
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }
}
