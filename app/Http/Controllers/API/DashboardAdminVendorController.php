<?php

namespace App\Http\Controllers\API;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardAdminVendorController extends APIController
{
    public function getName()
    {
        return $this->respondSuccess('Get name vendor is success', ['name' => auth()->user()->name]);
    }

    public function getIncomingOrder()
    {
        try {
            $vendorId = auth()->user()->id;
            $orders = Order::where('vendor_id', $vendorId)
                ->whereHas('orderDetails', function ($query) {
                    $query->where('order_status_id', 3);
                })
                ->count();
            return $this->respondSuccess('Get incoming order vendor is success', ['incoming_order' => $orders]);
        } catch (\Throwable $th) {
            return $this->respondInternalError($th->getMessage());
        }
    }
    public function getOngoingOrder()
    {
        try {
            $vendorId = auth()->user()->id;
            $statuses = [6, 7, 8];
            $orders = Order::where('vendor_id', $vendorId)
                ->whereHas('orderDetails', function ($query) use ($statuses) {
                    $query->whereIn('order_status_id', $statuses);
                })
                ->count();
            return $this->respondSuccess('Get ongoing order vendor is success', ['ongoing_order' => $orders]);
        } catch (\Throwable $th) {
            return $this->respondInternalError($th->getMessage());
        }
    }
    public function getFinishOrder()
    {
        try {
            $vendorId = auth()->user()->id;
            $orders = Order::where('vendor_id', $vendorId)
                ->whereHas('orderDetails', function ($query) {
                    $query->where('order_status_id', 9);
                })
                ->count();
            return $this->respondSuccess('Get finish order vendor is success', ['finish_order' => $orders]);
        } catch (\Throwable $th) {
            return $this->respondInternalError($th->getMessage());
        }
    }
    public function getCategoryOrder()
    {
        try {
            $vendorId = auth()->user()->id;
            $categoryOrderCounts = DB::table('categories')
                ->select('categories.name as category_name', DB::raw('COALESCE(COUNT(order_details.id), 0) as order_count'))
                ->leftJoin('products', 'categories.id', '=', 'products.category_id')
                ->leftJoin('order_details', 'products.id', '=', 'order_details.product_id')
                ->leftJoin('orders', function ($join) use ($vendorId) {
                    $join->on('order_details.order_id', '=', 'orders.id')
                        ->where('orders.vendor_id', '=', $vendorId);
                })
                ->groupBy('categories.name')
                ->orderBy('categories.id')
                ->get();

            // Extract order counts from the result
            $orderCounts = $categoryOrderCounts->pluck('order_count')->toArray();
            $orderCount = $categoryOrderCounts->pluck('category_name')->map(fn($label) => ucfirst($label))->toArray();

            // return $this->respondSuccess('Get category order vendor is success', $orderCounts);
            return $this->respondSuccess('Get category order vendor is success', ['chart_data' => ['labels' => $orderCount, 'values' => $orderCounts]]);
        } catch (\Throwable $th) {
            return $this->respondInternalError($th->getMessage());
        }
    }

    public function getTotalOrder()
    {
        try {
            $vendorId = auth()->user()->id;

            $startDate = Carbon::now()->subYear(); // Calculate the start date (1 year ago)
            $endDate = Carbon::now(); // Current date is the end date

            // Get total orders without grouping by month
            $totalOrders = Order::where('vendor_id', $vendorId)
                ->whereBetween('order_date', [$startDate, $endDate])
                ->count();

            // Get orders grouped by month
            $orders = Order::where('vendor_id', $vendorId)
                ->whereBetween('order_date', [$startDate, $endDate])
                ->selectRaw('MONTH(order_date) as month, COUNT(*) as total')
                ->groupBy('month')
                ->orderBy('month') // Optional: Order the results by month
                ->get()
                ->toArray(); // Convert the collection to an array

            // Generate labels and values for the chart
            $values = array_fill(1, 12, 0); // Initialize values for all months with 0

            foreach ($orders as $order) {
                $month = $order['month'];
                $values[$month] = $order['total']; // Update the value for the specific month
            }

            $chartData = [
                'labels' => array_keys($values),
                'values' => array_values($values), // Convert associative array to indexed array
            ];

            return $this->respondSuccess('Get total order vendor is success', ['total_order' => $totalOrders, 'chart_data' => $chartData]);
        } catch (\Throwable $th) {
            return $this->respondInternalError($th->getMessage());
        }
    }

    public function getTotalSelling()
    {
        try {
            $vendorId = auth()->user()->id;

            // Calculate start and end dates (last week)
            $startDate = Carbon::now()->subWeek()->endOfWeek()->subDay();
            $endDate = Carbon::now();
            // dd($startDate, $endDate);

            // Get total seling without grouping by day
            $totalSellingSum = DB::table('orders')
                ->join('payments', 'orders.transaction_code', '=', 'payments.transaction_code')
                ->where('orders.vendor_id', $vendorId)
                ->where('payments.status', 'settlement')
                ->whereBetween('payments.payment_date', [$startDate, $endDate])
                ->sum('payments.amount');

            // Get total selling grouped by day
            $selling = DB::table('orders')
                ->join('payments', 'orders.transaction_code', '=', 'payments.transaction_code')
                ->where('orders.vendor_id', $vendorId)
                ->where('payments.status', 'settlement')
                ->whereBetween('payments.payment_date', [$startDate, $endDate])
                ->selectRaw('DAYOFWEEK(payments.payment_date) as day, COALESCE(SUM(payments.amount), 0) as total_selling')
                ->groupBy('day')
                ->get();

            // Create an array to store all possible days of the week
            $allDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

            // Initialize values array with 0 for all days
            $values = array_fill(1, 7, 0);

            // Update values array with actual total selling values
            foreach ($selling as $item) {
                $values[$item->day] = (int) $item->total_selling;
            }

            $chartData = [
                'labels' => $allDays,
                'values' => array_values($values), // Convert associative array to indexed array
            ];

            return $this->respondSuccess('Get total selling vendor is success', ['total_selling' => $totalSellingSum, 'chart_data' => $chartData]);
            // return response()->json($response);
        } catch (\Throwable $th) {
            return $this->respondInternalError($th->getMessage());
        }
    }
}
