<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class TrackingController extends ApiController
{
    public function shipmentTracker(Request $request)
    {
        try {
            $request->validate([
                'courier' => 'required',
                'waybill' => 'required',
            ]);

            $courier = strtolower($request->courier);
            $waybill = $request->waybill;
            $apiKey = env('RAJAONGKIR_API_KEY');

            $apiUrl = "https://pro.rajaongkir.com/api/waybill";
            $client = new Client();
            try {
                $response = $client->request('POST', $apiUrl, [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'key' => $apiKey,
                    ],
                    'form_params' => [
                        'waybill' => $waybill,
                        'courier' => $courier,
                    ],
                ]);
            } catch (\Exception $e) {
                return $this->respondNotFound('Data shipment not found');
            }

            $result = json_decode($response->getBody(), true);
            if ($result['rajaongkir']['status']['code'] !== 200) {
                throw new \Exception($result['rajaongkir']['status']['description']);
            }

            $trackingDetails = [];
            foreach ($result['rajaongkir']['result']['manifest'] as $manifest) {
                $trackingDetails[] = [
                    'date' => $manifest['manifest_date'],
                    'time' => $manifest['manifest_time'],
                    'location' => $manifest['city_name'],
                    'message' => $manifest['manifest_description'],
                ];
            }

            return $this->respondSuccess('Data shipment found', [
                'courier' => $courier,
                'waybill' => $waybill,
                'tracking_details' => $trackingDetails,
            ]);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }
}
