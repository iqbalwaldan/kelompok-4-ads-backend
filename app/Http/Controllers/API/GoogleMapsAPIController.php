<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\GoogleMapsAPIRequest;
use App\Http\Resources\SearchGoogleMapsResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class GoogleMapsAPIController extends Controller
{
    private $key;

    public function __construct()
    {
        $this->key = config('app.gmaps_api_key');
    }

    public function getCoordinates(GoogleMapsAPIRequest $request)
    {
        $request->validated($request->all());
        $address = $request->address . $request->city . $request->province . $request->postal_code;

        $response = Http::get('https://maps.googleapis.com/maps/api/geocode/json', [
            'address' => $address,
            'key' => $this->key,
        ]);
        $data = $response->json();

        if ($response->ok() && $data['status'] === 'OK') {
            $location = $data['results'][0]['geometry']['location'];
            $latitude = $location['lat'];
            $longitude = $location['lng'];

            return response()->json(['latitude' => $latitude, 'longitude' => $longitude]);
        } else {
            return response()->json(['error' => 'Alamat tidak ditemukan'], 404);
        }
    }

    public function search(Request $request)
    {
        $address = $request->address;
        if ($address == null) {
            return response()->json([
                'status' => 'Not Found',
                'message' => 'Address not found.',
            ], 404);
        }

        $response = Http::get('https://maps.googleapis.com/maps/api/place/textsearch/json', [
            'query' => $address,
            'key' => $this->key,
        ]);

        return response()->json([
            'status' => 'Request was succesful',
            'message' => 'Address Found',
            'data' => new SearchGoogleMapsResource($response->json()),
        ], 200);
    }
}
