<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\RatingResource;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\RatingProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RatingController extends ApiController
{
    public function index($slug)
    {
        try {
            $product = Product::where('slug', $slug)->first();
            if ($product == null) {
                return $this->respondNotFound('Product was not found');
            }
            $rating = RatingProduct::where('product_id', $product->id)->get();
            if ($rating->isEmpty()) {
                return $this->respondNotFound('Rating product was not found');
            }
            return $this->respondSuccess('Product search was succesful', RatingResource::collection($rating));
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function show($slug, $id)
    {
        try {
            $product = Product::where('slug', $slug)->first();
            if ($product == null) {
                return $this->respondNotFound('Product was not found');
            }
            $rating = RatingProduct::where('product_id', $product->id)->where('id', $id)->first();
            if ($rating == null) {
                return $this->respondNotFound('Rating product was not found');
            }
            return $this->respondSuccess('Product search was succesful', new RatingResource($rating));
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'rating' => 'required|min:1|max:5',
                'comment' => 'required|string',
            ]);
            $user = auth()->user();
            if ($user->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                return $this->respondForbidden('Vendor cannot rate product');
            } else {
                $order = Order::where('id', $id)->where('user_id', $user->id)->first();
                if ($order == null) {
                    return $this->respondNotFound('Order was not found');
                }
                $orderDetail = OrderDetail::where('order_id', $order->id)->first();
            }
            if ($orderDetail->order_status_id != 9) {
                DB::rollBack();
                return $this->respondForbidden('Order status is not complete');
            }
            $ratings = RatingProduct::where('order_detail_id', $orderDetail->id)->get();
            if ($ratings->isNotEmpty()) {
                return $this->respondConflict('Rating was already exist');
            }
            if ($orderDetail == null) {
                return $this->respondNotFound('Order detail was not found');
            }
            $product = Product::find($orderDetail->product_id);
            if ($orderDetail == null) {
                return $this->respondNotFound('Order detail was not found');
            }
            $product->rating = ($product->rating + $request->rating) / 2;
            $product->save();
            RatingProduct::create([
                'order_detail_id' => $id,
                'product_id' => $orderDetail->product_id,
                'rating' => $request->rating,
                'comment' => $request->comment,
            ]);
            DB::commit();
            return $this->respondSuccess('Rating was succesful added');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function getSummaryRatingProduct($slug)
    {
        try {
            $product = Product::where('slug', $slug)->first();
            if ($product == null) {
                return $this->respondNotFound('Product was not found');
            }
            $ratings = RatingProduct::where('product_id', $product->id)->get();
            if ($ratings->isEmpty()) {
                return $this->respondSuccess('Rating product was not found', [
                    'rating' => 0,
                    'total_rating' => 0,
                    'rating_detail' => [
                        '1' => 0,
                        '2' => 0,
                        '3' => 0,
                        '4' => 0,
                        '5' => 0,
                    ],
                ]);
            }
            return $this->respondSuccess('Product search was succesful', [
                'rating' => $product->rating,
                'total_rating' => $ratings->count(),
                'rating_detail' => [
                    '1' => $ratings->where('rating', 1)->count(),
                    '2' => $ratings->where('rating', 2)->count(),
                    '3' => $ratings->where('rating', 3)->count(),
                    '4' => $ratings->where('rating', 4)->count(),
                    '5' => $ratings->where('rating', 5)->count(),
                ],
            ]);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }
}
