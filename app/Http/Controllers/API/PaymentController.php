<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;

class PaymentController extends ApiController
{

    public function __construct()
    {
        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = 'SB-Mid-server-oeqy_9DAzmfl_8fyFeAbDxW6';
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = false;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = false;
    }

    public function webhook(Request $request)
    {
        DB::beginTransaction();
        try {
            // otomatis 
            // $notif = new \Midtrans\Notification();

            // $transaction = $notif->transaction_status;
            // $fraud = $notif->fraud_status;

            // $order = Order::where('transaction_code', $notif->order_id)->firstOrFail();
            // $payment = Payment::where('id', $order->payment_id)->firstOrFail();

            // if ($transaction == 'capture') { // || $transaction == 'settlement'
            //     if ($fraud == 'challenge') {
            //         // TODO Set payment status in merchant's database to 'challenge'
            //         $payment->status = 'challenge';
            //     } else if ($fraud == 'accept') {
            //         // TODO Set payment status in merchant's database to 'success'
            //         $payment->status = 'capture';
            //     }
            // } else if ($transaction == 'cancel') {
            //     if ($fraud == 'challenge') {
            //         // TODO Set payment status in merchant's database to 'failure'
            //         $payment->status = 'challenge';
            //     } else if ($fraud == 'accept') {
            //         // TODO Set payment status in merchant's database to 'failure'
            //         $payment->status = 'cancel';
            //     }
            // } else if ($transaction == 'deny') {
            //     // TODO Set payment status in merchant's database to 'failure'
            //     $payment->status = 'deny';
            // }

            // Manual
            $auth = base64_encode(env('MIDTRANS_SERVER_KEY'));

            $request->validate([
                'transaction_code' => 'required',
            ]);

            $transaction_codes = explode(',', $request->transaction_code);
            // dd($transaction_codes);

            foreach ($transaction_codes as $transactionCode) {
                $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $auth,
                ])->get("https://api.sandbox.midtrans.com/v2/$transactionCode/status");

                $response = json_decode($response->body());

                $order = Order::where('transaction_code', $response->order_id)->firstOrFail();
                $orderDetail = OrderDetail::where('order_id', $order->id)->first();
                $payment = Payment::where('id', $order->payment_id)->firstOrFail();
                $oldStatus = $orderDetail->order_status_id;

                if ($payment->status === 'settlement' || $payment->status === 'capture') {
                    // Jika pembayaran sudah diproses, lewati iterasi ini
                    continue;
                }

                // Lakukan pemrosesan berdasarkan response Midtrans
                if ($response->transaction_status == 'capture') {
                    $payment->status = 'capture';
                    $orderDetail->order_status_id = 3;
                } elseif ($response->transaction_status == 'settlement') {
                    $payment->status = 'settlement';
                    $orderDetail->order_status_id = 3;
                } elseif ($response->transaction_status == 'pending') {
                    $payment->status = 'pending';
                    $orderDetail->order_status_id = 2;
                } elseif ($response->transaction_status == 'deny') {
                    $payment->status = 'deny';
                    $orderDetail->order_status_id = 10;
                } elseif ($response->transaction_status == 'expire') {
                    $payment->status = 'expire';
                    $orderDetail->order_status_id = 10;
                } elseif ($response->transaction_status == 'cancel') {
                    $payment->status = 'cancel';
                    $orderDetail->order_status_id = 10;
                }

                $payment->update([
                    'status' => $payment->status,
                ]);

                if ($oldStatus == 1 || $oldStatus == 2) {
                    $orderDetail->update([
                        'order_status_id' => $orderDetail->order_status_id,
                    ]);
                }
            }

            // $response = Http::withHeaders([
            //     'Content-Type' => 'application/json',
            //     'Authorization' => 'Basic ' . $auth,
            // ])->get("https://api.sandbox.midtrans.com/v2/$request->transaction_code/status");

            // $response = json_decode($response->body());

            // $order = Order::where('transaction_code', $response->order_id)->firstOrFail();
            // // dd($order);
            // $orderDetail = OrderDetail::where('order_id', $order->id)->first();
            // // dd($orderDetail);
            // $payment = Payment::where('id', $order->payment_id)->firstOrFail();
            // $oldStatus = $orderDetail->order_status_id;
            // if ($payment->status === 'settlement' || $payment->status === 'capture') {
            //     return $this->respondSuccess('Payment has been already processed');
            // }

            // if ($response->transaction_status == 'capture') {
            //     $payment->status = 'capture';
            //     $orderDetail->order_status_id = 3;
            // } else if ($response->transaction_status == 'settlement') {
            //     $payment->status = 'settlement';
            //     $orderDetail->order_status_id = 3;
            // } else if ($response->transaction_status == 'pending') {
            //     $payment->status = 'pending';
            //     $orderDetail->order_status_id = 2;
            // } else if ($response->transaction_status == 'deny') {
            //     $payment->status = 'deny';
            //     $orderDetail->order_status_id = 10;
            // } else if ($response->transaction_status == 'expire') {
            //     $payment->status = 'expire';
            //     $orderDetail->order_status_id = 10;
            // } else if ($response->transaction_status == 'cancel') {
            //     $payment->status = 'cancel';
            //     $orderDetail->order_status_id = 10;
            // }

            // $payment->update([
            //     'status' => $payment->status,
            // ]);
            // if ($oldStatus == 1 || $oldStatus == 2) {
            //     $orderDetail->update([
            //         'order_status_id' => $orderDetail->order_status_id,
            //     ]);
            // }
            DB::commit();
            return $this->respondSuccess('Payment status update succesful');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondInternalError($e->getMessage());
        }
    }
}
