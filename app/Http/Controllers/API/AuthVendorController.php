<?php

namespace App\Http\Controllers\API;

use App\Models\Link;
use App\Models\Vendor;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\LoginVendorRequest;
use App\Http\Requests\StoreVendorRequest;
use App\Http\Resources\VendorResource;
use Exception;
use Illuminate\Support\Facades\DB;

class AuthVendorController extends ApiController
{
    public function register(StoreVendorRequest $request)
    {
        DB::beginTransaction();
        try {
            $request->validated($request->all());
            $slug = Str::slug($request->name);
            $count = Vendor::where('slug', $slug)->count();
            if ($count > 0) {
                $slug = $slug . '-' . now()->timestamp;
            }
            $vendor = Vendor::create([
                'name' => $request->name,
                'dob' => $request->dob,
                'nik' => $request->nik,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'password' => Hash::make($request->password),
                'business_name' => $request->business_name,
                'slug' => $slug,
                'email_business' => $request->email_business,
                'phone_business' => $request->phone_business,
                'address' => $request->address,
                'province' => $request->province,
                'city' => $request->city,
                'postal_code' => $request->postal_code,
                'status' => "process",
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'know' => $request->know,
                'reason' => $request->reason,
            ]);

            if ($request->hasFile('ktp')) {
                $file = $request->file('ktp');
                $vendor->addMedia($file)->usingFileName(Str::random(16) . '.' . $file->getClientOriginalExtension())->toMediaCollection('vendor-ktp');
            }
            if ($request->hasFile('logo')) {
                $file = $request->file('logo');
                $vendor->addMedia($file)->usingFileName(Str::random(16) . '.' . $file->getClientOriginalExtension())->toMediaCollection('vendor-logo');
            }
            if ($request->hasFile('portofolio')) {
                $file = $request->file('portofolio');
                $vendor->addMedia($file)->usingFileName(Str::random(16) . '.' . $file->getClientOriginalExtension())->toMediaCollection('vendor-portofolio');
            }
            Link::create([
                'vendor_id' => $vendor->id,
                'name' => 'instagram',
                'url' => $request->instagram,
            ]);
            if ($request->website != null) {
                Link::create([
                    'vendor_id' => $vendor->id,
                    'name' => 'website',
                    'url' => $request->website,
                ]);
            }
            DB::commit();
            return $this->respondSuccess('Vendor register was succesful', new VendorResource($vendor));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function login(LoginVendorRequest $request)
    {
        try {
            if (!Auth::guard('vendor')->attempt($request->only('email', 'password'))) {
                return $this->respondInvalid('Invalid credentials');
            }
            $vendor = Vendor::where('email', $request->email)->firstOrFail();
            if ($vendor->status != "verified") {
                return $this->respondUnauthorized('Your account is still in the process of verification');
            }
            return $this->respondSuccess('Vendor login was succesful', ['vendor' => $vendor, 'token' => $vendor->createToken("API Token of " . $vendor->name)->plainTextToken]);
        } catch (Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function logout(Request $request)
    {
        try {
            $request->user()->currentAccessToken()->delete();
            return $this->respondSuccess('You have successfully been logged out.', null);
        } catch (Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }
}
