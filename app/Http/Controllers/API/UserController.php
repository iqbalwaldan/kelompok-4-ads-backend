<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;

class UserController extends ApiController
{
    public function show()
    {
        try {
            $user = auth()->user();
            if ($user->tokens->last()->tokenable_type != 'App\Models\User') {
                return $this->respondForbidden('You are not authorized to access this resource');
            }
            return $this->respondSuccess('User data was succesful', new UserResource($user));
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function index()
    {
        $users = User::all();
        return $this->respondSuccess('User list data was succesful', UserResource::collection($users));
    }

    public function update(Request $request){
        DB::beginTransaction();
        try{
            $user = auth()->user();
            if ($user->tokens->last()->tokenable_type != 'App\Models\User') {
                return $this->respondForbidden('You are not authorized to access this resource');
            }

            $request->validate([
                'photo_profile' => 'image|mimes:jpeg,png,jpg|max:2048',
                'first_name'    => 'required|string|max:255',
                'last_name'     => 'required|string|max:255',
                'email'         => 'required|string|email|max:255|unique:users,email,' . $user->id,
                'phone_number'  => 'required|string|max:255',
            ]);

            $user = User::where('id', $user->id)->first();
            $file = $request->file('photo_profile');
            if ($request->hasFile('photo_profile')) {
                $user->clearMediaCollection('user-photo-profile');
                $user->addMedia($file)->usingFileName(Str::random(16) . '.' . $file->getClientOriginalExtension())->toMediaCollection('user-photo-profile');
            }

            $user->update($request->all());

            DB::commit();
            return $this->respondSuccess('User data was succesful', new UserResource($user));
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function updatePassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = auth()->user();
            if ($user->tokens->last()->tokenable_type != 'App\Models\User') {
                return $this->respondForbidden('You are not authorized to access this resource');
            }
            $request->validate([
                'password_lama' => 'required|string',
                'password_baru' => 'required|string',
                'konfirmasi_password' => 'required|string|same:password_baru',
            ]);

            $user = User::where('id', $user->id)->first();

            if (!$user || !Hash::check($request->password_lama, $user->password)) {
                return $this->respondNotFound('You are not authorized to edit this data');
            }

            $hashedPassword = Hash::make($request->password_baru);
            $user->update(['password' => $hashedPassword]);

            DB::commit();
            return $this->respondSuccess('Vendor password was updated successfully', new UserResource($user));
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondInvalid($e->getMessage());
        }
    }
}
