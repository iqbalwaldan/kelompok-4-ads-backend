<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ImageResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProjectInfoResource;
use App\Http\Resources\UserListOrderResource;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Payment;
use App\Models\Product;
use App\Models\User;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;


class OrderController extends ApiController
{
    public function projectInfo($id)
    {
        try {
            $user = auth()->user();
            if ($user->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                $order = Order::where('id', $id)->where('vendor_id', $user->id)->firstOrFail();
            } else {
                $order = Order::where('id', $id)->where('user_id', $user->id)->firstOrFail();
            }
            $orderDetail = OrderDetail::where('order_id', $order->id)->firstOrFail();
            $product = Product::where('id', $orderDetail->product_id)->firstOrFail();
            $vendor = Vendor::where('id', $product->vendor_id)->firstOrFail();
            $photoProduct = collect();
            if ($product->getMedia('photo-product')) {
                foreach ($product->getMedia('photo-product') as $media) {
                    $photoProduct[] = new ImageResource($media);
                }
            }
        } catch (\Exception $e) {
            return $this->respondNotFound('Order not found', $e->getMessage());
        }

        if ($photoProduct->isEmpty()) {
            $photoProduct = [];
        } else {
            $photoProduct = $photoProduct[0];
        }

        $data = [
            'project_info' => [
                'image' => $photoProduct,
                'name' => $orderDetail->title,
                'transaction_number' => $order->transaction_code,
                'total' => $order->total_payment,
                'order_date' => $order->order_date,
                'estimated_date' => \Carbon\Carbon::parse($order->order_date)->addDays(3)->format('Y-m-d H:i:s')
            ],
            'vendor_info' => [
                'name' => $vendor->business_name,
                'address' => $vendor->address . ', ' . $vendor->city . ', ' . $vendor->province . ', ' . $vendor->postal_code,
            ],
        ];
        return $this->respondSuccess('Order found', $data);
    }

    public function store(Request $request, $slug)
    {
        DB::beginTransaction();
        try {
            $user = auth()->user();
            $product = Product::where('slug', $slug)->first();
            if ($product == null) {
                return response()->json([
                    'status'    => 'Not Found',
                    'message'   => 'Product was not found',
                ], 404);
            }

            try {
                $request->validate([
                    'quantity'  => 'required|integer|min:1',
                    'admin_fee' => 'required|integer|min:0',
                    'title'     => 'required|string',
                    'brief'     => 'required|string',
                    'link'      => 'required|string',
                ]);
            } catch (ValidationException $e) {
                return $this->respondInvalid($e->errors());
            }

            // Generate transaction_code
            $formattedDate = now()->format('Ymd');
            $formattedTime = now()->format('His');
            $uniquePart = str_replace('.', '', microtime(true));
            $transactionCode = "TRX" . $formattedDate . $formattedTime . $uniquePart;


            $order = Order::create([
                'user_id'       => $user->id,
                'vendor_id'     => $product->vendor_id,
                'transaction_code'  => $transactionCode,
                'order_date'    => now(),
            ]);

            $detail_order = OrderDetail::create([
                'order_id'          => $order->id,
                'product_id'        => $product->id,
                'quantity'          => $request->quantity,
                'price'             => $product->price,
                'admin_fee'         => $request->admin_fee,
                'subtotal'          => ($product->price * $request->quantity) + $request->admin_fee,
                'title'             => $request->title,
                'brief'             => $request->brief,
                'link'              => $request->link,
                'order_status_id'   => 1,
            ]);

            $order->update([
                'total_payment' => $order->total_payment + $detail_order->subtotal,
            ]);

            $params = array(
                'transaction_details' => array(
                    'order_id' => $order->transaction_code,
                    'gross_amount' => $order->total_payment,
                ),
                'item_details' => [
                    [
                        'id' => 0,
                        'price' => $request->admin_fee,
                        'quantity' => 1,
                        'name' => 'Admin Fee',
                        'subtotal' => $request->admin_fee,
                    ],
                    [
                        'id' => $product->id,
                        'price' => $product->price,
                        'quantity' => $request->quantity,
                        'name' => $product->name,
                        'subtotal' => $order->total_payment,
                    ],
                ],
                'customer_details' => array(
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'email' => $user->email,
                    'phone' => $user->phone_number,
                ),
                'enabled_payments' => array('credit_card', 'gopay', 'bca_va'),
            );

            $auth = base64_encode(env('MIDTRANS_SERVER_KEY'));

            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
                'Authorization' => 'Basic ' . $auth,
            ])->post(config('app.midtrans_link'), $params);

            $response = json_decode($response->body());

            $payment = Payment::create([
                'user_id' => $user->id,
                'transaction_code' => $order->transaction_code,
                'link' => $response->redirect_url,
                'amount' => $order->total_payment,
                'status' => 'pending',
                'payment_date' => now(),
            ]);

            $order->update([
                'payment_id' => $payment->id,
            ]);

            DB::commit();
            return $this->respondSuccess('Order was succesful', $response);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function acceptProject(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'accept' => 'required',
                'reason_reject' => 'string|max:255',
            ]);
            try {
                $user = auth()->user();
                if ($user->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                    $order = Order::where('id', $id)->where('vendor_id', $user->id)->firstOrFail();
                } else {
                    return $this->respondNotFound('Order not found');
                }
                $orderDetail = OrderDetail::where('order_id', $order->id)->firstOrFail();
                $product = Product::where('id', $orderDetail->product_id)->firstOrFail();
            } catch (\Exception $e) {
                return $this->respondNotFound('Order not found', $e->getMessage());
            }
            if ($request->accept == 1 && $orderDetail->order_status_id == 3) {
                if ($product->need_shipping == 1) {
                    $orderDetail->update([
                        'order_status_id' => 4,
                    ]);
                } else {
                    $orderDetail->update([
                        'order_status_id' => 6,
                    ]);
                }
                DB::commit();
                return $this->respondSuccess('Order accepted');
            } else if ($request->accept == 0 && $orderDetail->order_status_id == 3) {
                $orderDetail->update([
                    'order_status_id' => 10,
                    'reason_reject' => $request->reason_reject,
                ]);
                DB::commit();
                return $this->respondSuccess('Order rejected');
            } else {
                DB::rollBack();
                return $this->respondInternalError('This order status already updated');
            }
        } catch (\Exception $e) {
            db::rollBack();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function updateOrderShipment(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'shipment' => 'required|string',
                'resi' => 'required|string',
                'delivery-photo.*' => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            try {
                if (auth()->user()->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                    $order = Order::where('id', $id)->where('vendor_id', auth()->user()->id)->firstOrFail();
                } else {
                    $order = Order::where('id', $id)->where('user_id', auth()->user()->id)->firstOrFail();
                }
            } catch (\Exception $e) {
                return $this->respondNotFound('Order not found', $e->getMessage());
            }
            $orderDetail = OrderDetail::where('order_id', $order->id)->firstOrFail();
            if ($orderDetail->order_status_id != 4) {
                DB::rollBack();
                return $this->respondInternalError('Shipment send already updated');
            }
            $orderDetail->update([
                'shipment' => $request->shipment,
                'no_resi' => $request->resi,
                'order_status_id' => 5,
            ]);
            $order->update([
                'send_date' => now(),
            ]);
            if ($request->hasFile('delivery-photo')) {
                $order->clearMediaCollection('delivery-photo');
                foreach ($request->file('delivery-photo') as $evidence) {
                    $order->addMedia($evidence)->usingFileName(Str::random(16) . '.' . $evidence->getClientOriginalExtension())->toMediaCollection('delivery-photo');
                }
            }
            DB::commit();
            return $this->respondSuccess('Order updated');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function getOrderShipment(Request $request, $id)
    {
        try {
            $request->validate([
                'type' => 'required|boolean',
            ]);
            try {
                if (auth()->user()->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                    $order = Order::where('id', $id)->where('vendor_id', auth()->user()->id)->firstOrFail();
                } else {
                    $order = Order::where('id', $id)->where('user_id', auth()->user()->id)->firstOrFail();
                }
            } catch (\Exception $e) {
                return $this->respondNotFound('Order not found', $e->getMessage());
            }
            $vendor = Vendor::where('id', $order->vendor_id)->firstOrFail();
            $user = User::where('id', $order->user_id)->firstOrFail();
            $photoProduct = collect();
            if ($order->getMedia('delivery-photo') && $request->type == 0) {
                foreach ($order->getMedia('delivery-photo') as $media) {
                    $photoProduct[] = new ImageResource($media);
                }
            } else if ($order->getMedia('acceptance-photo') && $request->type == 1) {
                foreach ($order->getMedia('acceptance-photo') as $media) {
                    $photoProduct[] = new ImageResource($media);
                }
            }
            $data = [
                'sender' => $user->first_name . ' ' . $user->last_name,
                'receiver' => $vendor->business_name,
                'address' => $vendor->address . ', ' . $vendor->city . ', ' . $vendor->province . ', ' . $vendor->postal_code,
                'image' => $photoProduct,
            ];
            return $this->respondSuccess('Shipment Send found', $data);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function storeOrderShipmentRecipt(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'acceptance-photo.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            try {
                if (auth()->user()->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                    $order = Order::where('id', $id)->where('vendor_id', auth()->user()->id)->firstOrFail();
                } else {
                    $order = Order::where('id', $id)->where('user_id', auth()->user()->id)->firstOrFail();
                }
            } catch (\Exception $e) {
                return $this->respondNotFound('Order not found', $e->getMessage());
            }
            $orderDetail = OrderDetail::where('order_id', $order->id)->firstOrFail();
            if ($orderDetail->order_status_id != 5) {
                DB::rollBack();
                return $this->respondInternalError('Shipment recipt already updated');
            }
            $orderDetail->update([
                'received_date' => now(),
                'order_status_id' => 6,
            ]);
            if ($request->hasFile('acceptance-photo')) {
                $order->clearMediaCollection('acceptance-photo');
                foreach ($request->file('acceptance-photo') as $evidence) {
                    $order->addMedia($evidence)->usingFileName(Str::random(16) . '.' . $evidence->getClientOriginalExtension())->toMediaCollection('acceptance-photo');
                }
            }
            DB::commit();
            return $this->respondSuccess('Order Shipment Recipt success');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function requestRevision(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            try {
                if (auth()->user()->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                    $order = Order::where('id', $id)->where('vendor_id', auth()->user()->id)->firstOrFail();
                    $orderDetail = OrderDetail::where('order_id', $order->id)->firstOrFail();
                    $product = Product::where('id', $orderDetail->product_id)->firstOrFail();
                    if ($orderDetail->order_status_id == 8) {
                        $orderDetail->update([
                            'order_status_id' => 6,
                            'revision' => ($orderDetail->revision + 1),
                        ]);
                    } else {
                        DB::rollBack();
                        return $this->respondInternalError('This order status already updated');
                    }
                    $revision = $orderDetail->revision;
                } else {
                    $request->validate([
                        'revision' => 'required|boolean',
                    ]);
                    $order = Order::where('id', $id)->where('user_id', auth()->user()->id)->firstOrFail();
                    $orderDetail = OrderDetail::where('order_id', $order->id)->firstOrFail();
                    $product = Product::where('id', $orderDetail->product_id)->firstOrFail();
                    if ($orderDetail->revision == ($product->revision)) {
                        $orderDetail->update([
                            'order_status_id' => 9,
                        ]);
                        DB::commit();
                        return $this->respondSuccess('Order Was Finished');
                    }
                    if ($orderDetail->order_status_id == 7 && $orderDetail->revision < ($product->revision) && $request->revision == 1) {
                        $orderDetail->update([
                            'order_status_id' => 8,
                        ]);
                        $revision = $orderDetail->revision + 1;
                    } else if ($request->revision == 0) {
                        $orderDetail->update([
                            'order_status_id' => 9,
                        ]);
                        $revision = $orderDetail->revision;
                    } else {
                        DB::rollBack();
                        return $this->respondInternalError('This order status already updated');
                    }
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->respondNotFound('Order not found', $e->getMessage());
            }
            $status = config('data.order_status')[$orderDetail->order_status_id]['label'];
            DB::commit();
            return $this->respondSuccess('Order Revision Requested', ['status' => $status, 'number_of_revision' => $revision]);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function projectStatus($id)
    {
        try {
            $user = auth()->user();
            if ($user->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                $order = Order::where('id', $id)->where('vendor_id', $user->id)->firstOrFail();
            } else {
                $order = Order::where('id', $id)->where('user_id', $user->id)->firstOrFail();
            }
            $orderDetail = OrderDetail::where('order_id', $order->id)->firstOrFail();
        } catch (\Exception $e) {
            return $this->respondNotFound('Order not found', $e->getMessage());
        }

        $data = [
            'project_status' => [
                'status_id' => $orderDetail->order_status_id,
                'status' => config('data.order_status')[$orderDetail->order_status_id]['label'],
            ],
        ];
        return $this->respondSuccess('Order found', $data);
    }

    public function shipmentRecipt($id)
    {
        try {
            $user = auth()->user();
            if ($user->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                $order = Order::where('id', $id)->where('vendor_id', $user->id)->firstOrFail();
            } else {
                $order = Order::where('id', $id)->where('user_id', $user->id)->firstOrFail();
            }
            $orderDetail = OrderDetail::where('order_id', $order->id)->firstOrFail();
        } catch (\Exception $e) {
            return $this->respondNotFound('Shipment not found', $e->getMessage());
        }

        $data = [
            'shipment-recipt' => [
                'courier' => $orderDetail->shipment,
                'recipt' => $orderDetail->no_resi,
            ],
        ];
        return $this->respondSuccess('Shipment found', $data);
    }

    public function getProjectList()
    {
        try {
            $user = auth()->user();
            if ($user->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                $order = Order::where('vendor_id', $user->id)->get();
            } else {
                $order = Order::where('user_id', $user->id)->get();
            }
            return $this->respondSuccess('Order found', ProjectInfoResource::collection($order));
        } catch (\Exception $e) {
            return $this->respondNotFound('Order not found', $e->getMessage());
        }
    }
    public function getProject($id)
    {
        try {
            $user = auth()->user();
            if ($user->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                $order = Order::where('vendor_id', $user->id)->where('id', $id)->firstOrFail();
            } else {
                $order = Order::where('user_id', $user->id)->where('id', $id)->firstOrFail();
            }
            return $this->respondSuccess('Order found', new ProjectInfoResource($order));
        } catch (\Exception $e) {
            return $this->respondNotFound('Order not found', $e->getMessage());
        }
    }
}
