<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\Vendor;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\VendorResource;
use App\Http\Controllers\API\ApiController;
use App\Http\Resources\ImageResource;
use App\Http\Resources\PublicVendorResource;
use App\Models\Link;

class VendorController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return $this->respondSuccess('Success', PublicVendorResource::collection(Vendor::all()));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        try {
            $vendor = Vendor::where('slug', $request->user()->slug)->first();
            if ($vendor == null) {
                return $this->respondNotFound('Vendor data was not found');
            }
            if (auth()->user() == null) {

                return $this->respondSuccess('Vendor data was succesful', new PublicVendorResource($vendor));
            }
            $userType = auth()->user()->tokens->last()->tokenable_type;
            if ($userType == 'App\Models\Vendor') {
                return $this->respondSuccess('Vendor data was succesful vendor', new VendorResource($vendor));
            }
        } catch (\Exception $e) {
            return $this->respondInvalid($e->getMessage());
        }
    }

    public function publicShow(Request $request, $slug)
    {
        try {
            $vendor = Vendor::where('slug', $slug)->first();
            if ($vendor == null) {
                return $this->respondNotFound('Vendor data was not found');
            }
            if (auth()->user() == null) {

                return $this->respondSuccess('Vendor data was succesful', new PublicVendorResource($vendor));
            }
            $userType = auth()->user()->tokens->last()->tokenable_type;
            if ($userType == 'App\Models\Vendor') {
                return $this->respondSuccess('Vendor data was succesful vendor', new VendorResource($vendor));
            }
        } catch (\Exception $e) {
            return $this->respondInvalid($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|unique:users,email,' . $request->user()->id,
                'phone_number' => 'string|max:255',
            ]);
            $vendor = Vendor::where('slug', $request->user()->slug)->first();
            if ($vendor == null) {
                return $this->respondNotFound('You are not authorized to edit this data');
            }
            $vendor->update($request->all());

            DB::commit();
            return $this->respondSuccess('Vendor was updated succesfully', new VendorResource($vendor));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->respondInvalid($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function updatePassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $vendor = auth()->user();
            if ($vendor->tokens->last()->tokenable_type != 'App\Models\Vendor') {
                return $this->respondForbidden('You are not authorized to access this resource');
            }
            $request->validate([
                'password_lama' => 'required|string',
                'password_baru' => 'required|string',
                'konfirmasi_password' => 'required|string|same:password_baru',
            ]);

            $vendor = Vendor::where('id', $vendor->id)->first();

            if (!$vendor || !Hash::check($request->password_lama, $vendor->password)) {
                return $this->respondNotFound('You are not authorized to edit this data');
            }

            $hashedPassword = Hash::make($request->password_baru);
            $vendor->update(['password' => $hashedPassword]);

            DB::commit();
            return $this->respondSuccess('Vendor password was updated successfully', new VendorResource($vendor));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->respondInvalid($e->getMessage());
        }
    }

    public function updateStore(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'business_name' => 'string|max:255',
                'description' => 'string|max:100',
                'email_business' => 'string|email|unique:vendors,email_business,' . $request->user()->id,
                'phone_business' => 'string|max:255',
                'address' => 'string|max:255',
                'province' => 'string|max:255',
                'city' => 'string|max:255',
                'postal_code' => 'string|max:255',
                'latitude' => 'string|max:255',
                'longitude' => 'string|max:255',
                'instagram' => 'string|max:255',
                'website' => 'string|max:255',
                'portofolio' => 'string|max:255',
                'logo' => 'image|mimes:jpeg,png,jpg',
                'banner' => 'image|mimes:jpeg,png,jpg',
                // 'logo' => 'string',
                // 'banner' => 'string',
            ]);
            // authorization
            $vendor = Vendor::where('slug', $request->user()->slug)->first();
            if ($vendor == null) {
                return $this->respondNotFound('You are not authorized to edit this data');
            }

            // upload image
            if ($request->hasFile('logo')) {
                $file = $request->file('logo');
                $vendor->clearMediaCollection('vendor-logo');
                $vendor->addMedia($file)->usingFileName(Str::random(16) . '.' . $file->getClientOriginalExtension())->toMediaCollection('vendor-logo');
            }
            if ($request->hasFile('banner')) {
                $file = $request->file('banner');
                $vendor->clearMediaCollection('vendor-banner');
                $vendor->addMedia($file)->usingFileName(Str::random(16) . '.' . $file->getClientOriginalExtension())->toMediaCollection('vendor-banner');
            }

            // select image
            // if ($request->logo != null) {
            //     // slice sting to a rray
            //     $logo_id = explode(',', $request->logo);
            //     $mediaCollection = $vendor->getMedia('vendor-logo');
            //     foreach ($mediaCollection as $image) {
            //         // Periksa apakah ID media ada dalam array $logo_id
            //         if (!in_array($image->id, $logo_id)) {
            //             // Hapus media yang tidak ada dalam array $logo_id
            //             $vendor->deleteMedia($image->id);
            //         }
            //     }
            // }
            // if ($request->banner != null) {
            //     // slice sting to a rray
            //     $banner_id = explode(',', $request->banner);
            //     $mediaCollection = $vendor->getMedia('vendor-banner');
            //     foreach ($mediaCollection as $image) {
            //         // Periksa apakah ID media ada dalam array $logo_id
            //         if (!in_array($image->id, $banner_id)) {
            //             // Hapus media yang tidak ada dalam array $logo_id
            //             $vendor->deleteMedia($image->id);
            //         }
            //     }
            // }

            // edit slug
            $newSlug = Str::slug($request->business_name);
            $count = Vendor::where('slug', $newSlug)->count();
            if ($count > 0) {
                $newSlug = $newSlug . '-' . now()->timestamp;
            }
            $vendor = Vendor::where('slug', $request->user()->slug)->first();
            if ($vendor == null) {
                return $this->respondNotFound('Product was not found');
            }
            if ($request->business_name == $vendor->business_name) {
                $request->merge([
                    'slug' => $vendor->slug,
                ]);
            } else {
                $request->merge([
                    'slug' => $newSlug,
                ]);
            }

            // update link
            if ($request->instagram != null) {
                $link = Link::where('vendor_id', $vendor->id)->where('name', 'instagram')->first();
                if ($link == null) {
                    Link::create([
                        'vendor_id' => $vendor->id,
                        'name' => 'instagram',
                        'url' => $request->instagram,
                    ]);
                } else {
                    $link->update([
                        'url' => $request->instagram,
                    ]);
                }
            }
            if ($request->website != null) {
                $link = Link::where('vendor_id', $vendor->id)->where('name', 'website')->first();
                if ($link == null) {
                    Link::create([
                        'vendor_id' => $vendor->id,
                        'name' => 'website',
                        'url' => $request->website,
                    ]);
                } else {
                    $link->update([
                        'url' => $request->website,
                    ]);
                }
            }
            if ($request->portofolio != null) {
                $link = Link::where('vendor_id', $vendor->id)->where('name', 'portofolio')->first();
                if ($link == null) {
                    Link::create([
                        'vendor_id' => $vendor->id,
                        'name' => 'portofolio',
                        'url' => $request->portofolio,
                    ]);
                } else {
                    $link->update([
                        'url' => $request->portofolio,
                    ]);
                }
            }
            $vendor->update($request->all());
            DB::commit();
            return $this->respondSuccess('Vendor was updated succesfully', new VendorResource($vendor));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->respondInvalid($e->getMessage());
        }
    }

    public function uploadFile(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'file' => 'required|image|mimes:jpeg,png,jpg',
                'type' => 'required|string',
            ]);
            $vendor = Vendor::where('slug', $request->user()->slug)->first();
            if ($vendor == null) {
                return $this->respondNotFound('You are not authorized to edit this data');
            }
            if ($request->type == '1') {
                // $vendor->clearMediaCollection('vendor-logo');
                $vendor->addMedia($request->file)->usingFileName(Str::random(16) . '.' . $request->file->getClientOriginalExtension())->toMediaCollection('vendor-logo');
                $image = $vendor->getMedia('vendor-logo')->last();
            } else if ($request->type == '2') {
                // $vendor->clearMediaCollection('vendor-banner');
                $vendor->addMedia($request->file)->usingFileName(Str::random(16) . '.' . $request->file->getClientOriginalExtension())->toMediaCollection('vendor-banner');
                $image = $vendor->getMedia('vendor-banner')->last();
            } else if ($request->type == '3') {
                // $vendor->clearMediaCollection('vendor-ktp');
                $vendor->addMedia($request->file)->usingFileName(Str::random(16) . '.' . $request->file->getClientOriginalExtension())->toMediaCollection('vendor-ktp');
                $image = $vendor->getMedia('vendor-ktp')->last();
            } else if ($request->type == '4') {
                // $vendor->clearMediaCollection('vendor-portofolio');
                $vendor->addMedia($request->file)->usingFileName(Str::random(16) . '.' . $request->file->getClientOriginalExtension())->toMediaCollection('vendor-portofolio');
                $image = $vendor->getMedia('vendor-portofolio')->last();
            } else {
                return $this->respondInvalid('Type is not valid');
            }

            DB::commit();
            return $this->respondSuccess('Vendor was updated succesfully', new ImageResource($image));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->respondInvalid($e->getMessage());
        }
    }
}
