<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ProjectChatResource;
use App\Models\Link;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ProjectChat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectChatController extends APIController
{
    public function store(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                // 'order_id' => 'required|exists:orders,id',
                'link' => 'string',
                'message' => 'required|string',
            ]);

            $user = auth()->user();
            if ($user->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                $order = Order::where('id', $id)->where('vendor_id', $user->id)->first();
                if ($order == null) {
                    return $this->respondNotFound('Order not found');
                }
                $orderDetail = OrderDetail::where('order_id', $order->id)->firstOrFail();

                if (6 > $orderDetail->order_status_id || $orderDetail->order_status_id > 8) {
                    DB::rollback();
                    return $this->respondUnauthorized('You are not authorized to access send message');
                }

                $revision = $orderDetail->revision;
                if ($revision == null) {
                    $revision = 0;
                }

                $message = $request->message;
                if ($request->link) {
                    $message = $request->link . "\n" . $request->message;
                }
                $chat = ProjectChat::create([
                    'order_id' => $id,
                    'user_id' => $order->user_id,
                    'vendor_id' => $order->vendor_id,
                    'revision' => $revision,
                    'message' => $message,
                    'sender' => 'vendor',
                ]);
            } else {
                $order = Order::where('id', $id)->where('user_id', $user->id)->first();
                if ($order == null) {
                    return $this->respondNotFound('Order not found');
                }
                $orderDetail = OrderDetail::where('order_id', $order->id)->firstOrFail();

                if (7 > $orderDetail->order_status_id || $orderDetail->order_status_id > 8) {
                    DB::rollback();
                    return $this->respondUnauthorized('You are not authorized to access send message');
                }

                $revision = $orderDetail->revision;
                if ($revision == null) {
                    $revision = 0;
                }

                $chat = ProjectChat::create([
                    'order_id' => $id,
                    'user_id' => $order->user_id,
                    'vendor_id' => $order->vendor_id,
                    'revision' => $revision,
                    'message' => $request->message,
                    'sender' => 'user',
                ]);
            }

            if ($orderDetail->order_status_id == 6) {
                $orderDetail->update([
                    'order_status_id' => 7,
                ]);
            }

            DB::commit();
            return $this->respondSuccess('Chat created successfully', $chat);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function listMessage(Request $request, $id)
    {
        try {
            $request->validate([
                'send_project' => 'integer',
            ]);
            $user = auth()->user();
            // $order = Order::where('id', $id)->firstOrFail();
            // $orderDetail = OrderDetail::where('order_id', $order->id)->firstOrFail();
            if ($user->tokens->last()->tokenable_type == 'App\Models\Vendor') {
                $order = Order::where('id', $id)->where('vendor_id', auth()->user()->id)->first();
                if ($order == null) {
                    return $this->respondNotFound('Order not found');
                }
                $revision = $request->send_project - 1;
                $chat = ProjectChat::where('vendor_id', $user->id)->where('order_id', $id)->where('revision', $revision)->get();
                if ($chat->isEmpty()) {
                    return $this->respondNotFound('Chat not found');
                }
                return $this->respondSuccess('List chat vendor', ProjectChatResource::collection($chat));
            } else {
                $order = Order::where('id', $id)->where('user_id', auth()->user()->id)->first();
                if ($order == null) {
                    return $this->respondNotFound('Order not found');
                }
                $revision = $request->send_project - 1;
                $chat = ProjectChat::where('user_id', $user->id)->where('order_id', $id)->where('revision', $revision)->get();
                if ($chat->isEmpty()) {
                    return $this->respondNotFound('Chat not found');
                }
                return $this->respondSuccess('List chat user', ProjectChatResource::collection($chat));
            }
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }
}
