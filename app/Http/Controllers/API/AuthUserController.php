<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Password;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\DB;

class AuthUserController extends ApiController
{
    public function loginRequired()
    {
        return $this->respondForbidden('Log in first to access the resources.');
    }
    public function register(StoreUserRequest $request)
    {
        DB::beginTransaction();
        try {
            $request->validated($request->all());

            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'password' => Hash::make($request->password),
                'isAdmin' => false
            ]);

            DB::commit();
            return $this->respondSuccess('User register was succesful', $user);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->respondInvalid($e->getMessage());
        }
    }

    public function login(LoginUserRequest $request)
    {
        try {
            $request->validated($request->all());

            if (!Auth::attempt($request->only('email', 'password'))) {
                return $this->respondInvalid('Invalid credentials');
            }
            $user = User::where('email', $request->email)->firstOrFail();

            return $this->respondSuccess('User login was succesful', ['user' => new UserResource($user), 'token' => $user->createToken("API Token of " . $user->first_name . " " . $user->last_name)->plainTextToken]);
        } catch (Exception $e) {
            return $this->respondInvalid($e->getMessage());
        }
    }

    public function logout(Request $request)
    {
        try {
            $request->user()->currentAccessToken()->delete();
            return $this->respondSuccess('You have successfully been logged out.', null);
        } catch (Exception $e) {
            return $this->respondInvalid($e->getMessage());
        }
    }

    public function update(Request $request)
    {
        try {
            $user = $request->user();
            $user->update($request->all());

            return $this->respondSuccess('User was updated succesfully', $user);
        } catch (Exception $e) {
            return $this->respondInvalid($e->getMessage());
        }
    }

    public function redirectToGoole()
    {
        return response()->json([
            'status' => 'Request was succesful',
            'message' => 'Redirect to google was succesful',
            'data' => Socialite::driver('google')->stateless()->redirect()->getTargetUrl()
        ], 200);
        // return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        $user = Socialite::driver('google')->stateless()->user();
        $finduser = User::where('email', $user->email)->first();
        $cookie_name = 'token';
        $cookieExpiration = time() + 60 * 60 * 24;
        if ($finduser) {
            $data = $finduser->createToken("API Token of " . $user->name)->plainTextToken;
            if (!isset($_COOKIE[$cookie_name])) {
                setcookie($cookie_name, json_encode($data), $cookieExpiration, "/");
            }
            return redirect(config('app.url_frontend'));
        } else {
            $user = User::create([
                'first_name' => $user->user['given_name'],
                'last_name' => $user->user['family_name'],
                'email' => $user->email,
                'password' => Hash::make(Str::random(10)),
                'isAdmin' => false
            ]);

            $data = $user->createToken("API Token of " . $user->name)->plainTextToken;
            if (!isset($_COOKIE[$cookie_name])) {
                setcookie($cookie_name, json_encode($data), $cookieExpiration, "/");
            }
            return redirect(config('app.url_frontend'));
        }
    }

    public function forgotPassword(Request $request)
    {
        try {
            $request->validate(['email' => 'required|email']);

            $status = Password::sendResetLink(
                $request->only('email')
            );

            return $status === Password::RESET_LINK_SENT
                ? $this->respondSuccess(__($status), null)
                : $this->respondNotFound(__($status), null);
        } catch (Exception $e) {
            return $this->respondInvalid($e->getMessage());
        }
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        try {
            $request->validated($request->all());

            $status = Password::reset(
                $request->only('email', 'password', 'password_confirmation', 'token'),
                function (User $user, string $password) {
                    $user->forceFill([
                        'password' => Hash::make($password)
                    ])->setRememberToken(Str::random(60));

                    $user->save();

                    event(new PasswordReset($user));
                }
            );

            return $status === Password::PASSWORD_RESET
                ? $this->respondSuccess(__($status), null)
                : $this->respondUnauthorized(__($status), null);
        } catch (Exception $e) {
            return $this->respondInvalid($e->getMessage());
        }
    }
}
