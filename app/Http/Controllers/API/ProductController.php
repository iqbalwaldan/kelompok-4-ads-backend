<?php

namespace App\Http\Controllers\API;

use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;
use App\Http\Requests\StoreProductRequest;
use App\Http\Resources\ImageResource;
use App\Models\Vendor;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\returnCallback;

class ProductController extends ApiController
{
    public function listProduct()
    {
        try {

            $data = ProductResource::collection(Product::where('status', 'Publish')->paginate(6));
            if ($data->isEmpty()) {
                return $this->respondNotFound('Product list was not found');
            }
            return $this->respondSuccess('Product list was found', $data);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function listProductVendor($slug)
    {
        try {
            $vendor = Vendor::where('slug', $slug)->first();
            if ($vendor == null) {
                return $this->respondNotFound('Vendor was not found');
            }
            $data = ProductResource::collection(Product::where('status', 'Publish')->where('vendor_id', $vendor->id)->paginate(6));
            if ($data->isEmpty()) {
                return $this->respondNotFound('Product list was not found');
            }
            return $this->respondSuccess('Product list was found', $data);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function searchBySlugProduct($slug)
    {
        try {
            $product = Product::where('status', 'Publish')->where('slug', $slug)->first();
            if ($product == null) {
                return $this->respondNotFound('Product was not found');
            }
            return $this->respondSuccess('Product was found', new ProductResource($product));
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function listVendorProduct()
    {
        try {
            $data = ProductResource::collection(Product::where('vendor_id', auth()->user()->id)->get());
            if ($data->isEmpty()) {
                return $this->respondNotFound('Vendor product list was not found');
            }
            return $this->respondSuccess('Vendor product list was found', $data);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function searchBySlugVendorProduct($slug)
    {
        try {
            $product = Product::where('slug', $slug)->where('vendor_id', auth()->user()->id)->first();
            if ($product == null) {
                return $this->respondNotFound('Product was not found');
            }
            return $this->respondSuccess('Product was found', new ProductResource($product));
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function store(StoreProductRequest $request)
    {
        DB::beginTransaction();
        try {
            $request->validated($request->all());

            $slug = Str::slug($request->name);
            $count = Product::where('slug', $slug)->count();
            if ($count > 0) {
                $slug = $slug . '-' . now()->timestamp;
            }

            $product = Product::create([
                'vendor_id' => (string) auth()->user()->id,
                'category_id' => $request->category_id,
                'name' => $request->name,
                'slug' => $slug,
                'description' => $request->description,
                'price' => $request->price,
                'rating' => 0.0,
                'duration' => $request->duration,
                'revision' => $request->revision,
                'need_shipping' => $request->need_shipping,
            ]);
            if ($request->hasFile('photo-product')) {
                foreach ($request->file('photo-product') as $evidence) {
                    $product->addMedia($evidence)->usingFileName(Str::random(16) . '.' . $evidence->getClientOriginalExtension())->toMediaCollection('photo-product');
                }
            }
            DB::commit();
            return $this->respondSuccess('Product store was succesful', new ProductResource($product));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function update(Request $request, $slug)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'name' => 'required|string|max:255',
                'category_id' => 'integer',
                'description' => 'string',
                'price' => 'integer|min:0',
                // 'photo-product.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'photo_product' => 'string',
                'duration' => 'integer|min:0',
                'revision' => 'integer|min:0',
                'need_shipment' => 'boolean',
            ]);
            $newSlug = Str::slug($request->name);
            $count = Product::where('slug', $newSlug)->count();
            if ($count > 0) {
                $newSlug = $newSlug . '-' . now()->timestamp;
            }

            $product = Product::where('slug', $slug)->first();
            if ($product == null) {
                return $this->respondNotFound('Product was not found');
            }

            // select image
            if ($request->photo_product != null) {
                // slice sting to a rray
                $logo_id = explode(',', $request->photo_product);
                $mediaCollection = $product->getMedia('photo-product');
                foreach ($mediaCollection as $image) {
                    // Periksa apakah ID media ada dalam array $logo_id
                    if (!in_array($image->id, $logo_id)) {
                        // Hapus media yang tidak ada dalam array $logo_id
                        $product->deleteMedia($image->id);
                    }
                }
            }

            if ($request->name == $product->name) {
                $request->merge([
                    'slug' => $product->slug,
                ]);
            } else {
                $request->merge([
                    'slug' => $newSlug,
                ]);
            }
            if ($product->vendor_id != auth()->user()->id) {
                $this->respondUnauthorized('You are not authorized to edit this product');
            }

            $product->update($request->all());
            // if ($request->hasFile('photo-product')) {
            //     $product->clearMediaCollection('photo-product');
            //     foreach ($request->file('photo-product') as $evidence) {
            //         $product->addMedia($evidence)->usingFileName(Str::random(16) . '.' . $evidence->getClientOriginalExtension())->toMediaCollection('photo-product');
            //     }
            // }

            DB::commit();
            return $this->respondSuccess('Product update was succesful', new ProductResource($product));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function destroy($slug)
    {
        try {
            $product = Product::where('slug', $slug)->first();
            if ($product == null) {
                return $this->respondNotFound('Product was not found');
            }
            if ($product->vendor_id != auth()->user()->id) {
                return $this->respondUnauthorized('You are not authorized to delete this product');
            }
            $product->delete();
            return $this->respondSuccess('Product delete was succesful', new ProductResource($product));
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function deleteImage(Request $request, $slug)
    {
        try {
            $product = Product::where('slug', $request->slug)->first();
            if ($product->vendor_id != auth()->user()->id) {
                return $this->respondUnauthorized('You are not authorized to delete image this product');
            }
            foreach ($request->image_id as $image_id) {
                if ($product->getMedia('photo-product')->where('id', $image_id)->count() == 0) {
                    return $this->respondNotFound('Image was not found');
                }
                $product->deleteMedia($image_id);
            }
            return $this->respondSuccess('Image delete was succesful', $request->image_id);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function filterProduct(Request $request)
    {
        try {
            $products = collect();

            $productsQuery = Product::query();

            if ($request->has('category_id')) {
                $categoryIds = $request->input('category_id');
                $productsQuery->whereIn('category_id', $categoryIds);
            }

            if ($request->has('price')) {
                $price = $request->input('price');
                $productsQuery->whereBetween('price', $price);
            }

            if ($request->has('rating')) {
                $rating = $request->input('rating');
                $productsQuery->whereIn('rating', $rating);
            }

            if ($request->has('location')) {
                $location = $request->input('location');
                $productsQuery->whereHas('vendor', function ($query) use ($location) {
                    $query->whereIn('province', $location);
                });
            }

            if ($request->has('name')) {
                $name = $request->input('name');
                $productsQuery->where('name', 'like', '%' . $name . '%');
            }

            $productsQuery->with('vendor');

            $products = $productsQuery->get();

            if ($products->isEmpty()) {
                return $this->respondNotFound('No product found');
            }
            return $this->respondSuccess('Product found', ProductResource::collection($products));
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function popularProduct()
    {
        try {
            $data = Product::where('status', 'Publish')->orderBy('rating', 'desc')->get();
            if ($data->isEmpty()) {
                return $this->respondNotFound('Product list was not found');
            }
            return $this->respondSuccess('Product list was succesful', ProductResource::collection($data));
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function getProductImage($slug)
    {
        try {
            $product = Product::where('slug', $slug)->where('vendor_id', auth()->user()->id)->first();
            if ($product == null) {
                return $this->respondNotFound('Product was not found');
            }

            $productImage = collect();
            if ($product->getMedia('photo-product')) {
                foreach ($product->getMedia('photo-product') as $media) {
                    $productImage[] = new ImageResource($media);
                }
            }

            if ($productImage->isEmpty()) {
                return $this->respondNotFound('Product image was not found');
            }

            return $this->respondSuccess('Product image was found', $productImage);
        } catch (\Exception $e) {
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function updateStatus($slug)
    {
        DB::beginTransaction();
        try {
            $product = Product::where('slug', $slug)->first();
            if ($product == null) {
                return $this->respondNotFound('Product was not found');
            }
            if ($product->vendor_id != auth()->user()->id) {
                return $this->respondUnauthorized('You are not authorized to update this product');
            }
            if ($product->status == 'Publish') {
                $product->update([
                    'status' => 'Nonaktif'
                ]);
            } else {
                $product->update([
                    'status' => 'Publish'
                ]);
            }
            DB::commit();
            return $this->respondSuccess('Product status update was succesful', new ProductResource($product));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondInternalError($e->getMessage());
        }
    }

    public function uploadFile(Request $request, $slug)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'file.*' => 'required|image|mimes:jpeg,png,jpg',
                'type' => 'required|string',
            ]);
            // $vendor = Vendor::where('slug', $request->user()->slug)->first();
            $product = Product::where('slug', $slug)->where('vendor_id', auth()->user()->id)->first();
            if ($product == null) {
                return $this->respondNotFound('You are not authorized to edit this data');
            }
            if ($request->type == '1') {
                // $product->addMedia($request->file)->usingFileName(Str::random(16) . '.' . $request->file->getClientOriginalExtension())->toMediaCollection('photo-product');
                foreach ($request->file('file') as $evidence) {
                    $product->addMedia($evidence)->usingFileName(Str::random(16) . '.' . $evidence->getClientOriginalExtension())->toMediaCollection('photo-product');
                }
                $image = $product->getMedia('photo-product');
                // dd($image);
            } else {
                return $this->respondInvalid('Type is not valid');
            }

            DB::commit();
            return $this->respondSuccess('Vendor was updated succesfully', ImageResource::collection($image));
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respondInvalid($e->getMessage());
        }
    }
}
