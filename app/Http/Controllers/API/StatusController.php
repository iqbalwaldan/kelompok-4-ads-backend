<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Dflydev\DotAccessData\Data;
use Illuminate\Http\Request;

class StatusController extends APIController
{
    public function index(){
        $data = config('data.order_status');
        return $this->respondSuccess('Get status successfully',$data );
    }
}
