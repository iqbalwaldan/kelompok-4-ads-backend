<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\AddressResource;
use App\Http\Resources\ProvinceResource;
use Illuminate\Http\Request;
use Laravolt\Indonesia\Models\City;
use Laravolt\Indonesia\Models\District;
use Laravolt\Indonesia\Models\Province;
use Laravolt\Indonesia\Models\Village;

class AddressController extends Controller
{
    public function province()
    {
        $name = request('name');
        if ($name) {
            $province = Province::where('name', 'like', '%' . $name . '%')->get();
        } else {
            $province = Province::all();
        }
        if($province->isEmpty()) {
            return response()->json([
                'status' => 'Request was succesful',
                'message' => 'Data province not found',
            ], 404);
        }
        return response()->json([
            'status' => 'Request was succesful',
            'message' => 'Show data province',
            'data' => AddressResource::collection($province),
        ], 200);
    }
    public function city(Request $request)
    {
        $name = request('name');
        if ($name) {
            $city = City::where('province_code', $request->province_code)->where('name', 'like', '%' . $name . '%')->get();
        } else {
            $city = City::where('province_code', $request->province_code)->get();
        }
        if($city->isEmpty()) {
            return response()->json([
                'status' => 'Request was succesful',
                'message' => 'Data city not found',
            ], 404);
        }
        return response()->json([
            'status' => 'Request was succesful',
            'message' => 'Show data city',
            'data' => AddressResource::collection($city),
        ], 200);
    }
    public function district(Request $request)
    {
        $name = request('name');
        if ($name) {
            $district = District::where('city_code', $request->city_code)->where('name', 'like', '%' . $name . '%')->get();
        } else {
            $district = District::where('city_code', $request->city_code)->get();
        }
        if($district->isEmpty()) {
            return response()->json([
                'status' => 'Request was succesful',
                'message' => 'Data district not found',
            ], 404);
        }
        return response()->json([
            'status' => 'Request was succesful',
            'message' => 'Show data district',
            'data' => AddressResource::collection($district),
        ], 200);
    }
    public function village(Request $request)
    {
        $name = request('name');
        if ($name) {
            $village = Village::where('district_code', $request->district_code)->where('name', 'like', '%' . $name . '%')->get();
        } else {
            $village = Village::where('district_code', $request->district_code)->get();
        }
        if($village->isEmpty()) {
            return response()->json([
                'status' => 'Request was succesful',
                'message' => 'Data village not found',
            ], 404);
        }
        return response()->json([
            'status' => 'Request was succesful',
            'message' => 'Show data village',
            'data' => AddressResource::collection($village),
        ], 200);
    }
}
