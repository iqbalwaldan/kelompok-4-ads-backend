<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    // public function orderStatus()
    // {
    //     return $this->belongsTo(OrderStatus::class);
    // }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function ratingProduct()
    {
        return $this->hasMany(RatingProduct::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
