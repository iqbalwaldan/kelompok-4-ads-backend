<?php

namespace App\Models;

use App\Models\Link;
use App\Models\Product;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Rappasoft\LaravelAuthenticationLog\Traits\AuthenticationLoggable;

class Vendor extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, Notifiable, InteractsWithMedia, AuthenticationLoggable;
    protected $guarded = [
        'id'
    ];

    protected $hidden = [
        'password',
    ];

    public function address(): MorphOne
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    // hasMany
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function link()
    {
        return $this->belongsTo(Link::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function wallet()
    {
        return $this->hasOne(VendorWallet::class);
    }

    public function projectChat()
    {
        return $this->hasMany(ProjectChat::class);
    }
}
