<?php

return[
    'order_status' => [
        '1' => [
            'label' => 'Pesanan Baru',
            'note'  => 'Pesanan baru',
        ],
        '2' => [
            'label' => 'Menunggu Pembayaran',
            'note'  => 'Menunggu pembayaran',
        ],
        '3' => [
            'label' => 'Menunggu Konfirmasi',
            'note'  => 'Menunggu konfirmasi',
        ],
        '4' => [
            'label' => 'Menunggu Pengiriman',
            'note'  => 'Menunggu pengiriman',
        ],
        '5' => [
            'label' => 'Pengiriman',
            'note'  => 'Proses pengiriman',
        ],
        '6' => [
            'label' => 'Pengerjaan',
            'note'  => 'Proses pengerjaan',
        ],
        '7' => [
            'label' => 'Menunggu Review',
            'note'  => 'Menunggu konfirmasi review',
        ],
        '8' => [
            'label' => 'Menunggu Revisi',
            'note'  => 'Menunggu konfirmasi revisi',
        ],
        '9' => [
            'label' => 'Selesai',
            'note'  => 'Pesanan selesai',
        ],
        '10' => [
            'label' => 'Dibatalkan',
            'note'  => 'Pesanan dibatalkan',
        ],
    ]
];