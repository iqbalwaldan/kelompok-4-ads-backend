<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthUserController;
use App\Http\Controllers\Client\Admin\VendorAdminController;
use App\Http\Controllers\Client\Admin\AuthAdminController;
use App\Http\Controllers\Client\Admin\DashboardController;
use App\Http\Controllers\Client\Admin\LoginAdminController;
use App\Http\Controllers\Client\Admin\UserAdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('guest')->group(function () {
    Route::get('/login/google', [AuthUserController::class, 'redirectToGoole']);
    Route::get('/login/google/callback', [AuthUserController::class, 'handleGoogleCallback']);
});

// admin
Route::middleware('guest')->group(function () {
    Route::get('/login', [LoginAdminController::class, 'index']);
    Route::post('/login', [LoginAdminController::class, 'login'])->name('loginAdmin');
    // Route::get('/register', [AuthAdminController::class, 'register']);
});
Route::middleware('admin')->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/logout', [LoginAdminController::class, 'logout']);
    Route::get('/vendor', [VendorAdminController::class, 'index'])->name('vendor');
    Route::get('/vendor/show/{vendor:slug}', [VendorAdminController::class, 'show']);
    Route::post('/vendor/update/{vendor}', [VendorAdminController::class, 'update']);
    
    Route::get('/user', [UserAdminController::class, 'index'])->name('user');
    Route::get('/user/delete/{user}', [UserAdminController::class, 'delete']);
});

Route::get('/top-users', [DashboardController::class,'getTopUsers'])->name('top-users');
Route::get('/getTransactionProduct', [DashboardController::class, 'getTransactionProduct']);