<?php

use App\Http\Controllers\API\AddressController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\VendorController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\AuthUserController;
use App\Http\Controllers\API\AuthVendorController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\DashboardAdminVendorController;
use App\Http\Controllers\API\GoogleMapsAPIController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\PaymentController;
use App\Http\Controllers\API\ProjectChatController;
use App\Http\Controllers\API\RatingController;
use App\Http\Controllers\API\StatusController;
use App\Http\Controllers\API\TrackingController;
use App\Http\Controllers\API\UserController;
use App\Models\Vendor;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('guest')->group(function () {
    Route::controller(AuthUserController::class)->group(function () {
        Route::get('/login', 'loginRequired')->name('login');
        Route::post('/register', 'register');
        Route::post('/login', 'login');
        Route::get('/login/google', 'redirectToGoole');
        Route::get('/login/google/callback', 'handleGoogleCallback');
        Route::post('/forgot-password', 'forgotPassword');
        Route::post('/reset-password', 'resetPassword');
    });

    Route::controller(AuthVendorController::class)->prefix('vendor')->group(function () {
        Route::post('/register', 'register');
        Route::post('/login', 'login');
        // Route::get('/login/google', 'redirectToGoole');
        // Route::get('/login/google/callback', 'handleGoogleCallback');`
        // Route::post('/forgot-password', 'forgotPassword');
        // Route::post('/reset-password', 'resetPassword');
    });

    // forgot password
    Route::get('/reset-password/{token}', function (string $token) {
        // return view('auth.reset-password', ['token' => $token]);
        return response()->json([
            'status' => 'Request was succesful',
            'message' => null,
            'data' => $token
        ], 200);
    })->name('password.reset');


    Route::controller(AddressController::class)->group(function () {
        Route::post('/province', 'province');
        Route::post('/city', 'city');
        Route::post('/district', 'district');
        Route::post('/village', 'village');
    });

    Route::post('/location', [GoogleMapsAPIController::class, 'getCoordinates']);
    Route::post('/location/search', [GoogleMapsAPIController::class, 'search']);

    // new
    Route::controller(RatingController::class)->prefix('product/rating')->group(function () {
        Route::get('/{slug}', 'index');
        Route::get('/summary/{slug}', 'getSummaryRatingProduct');
        Route::get('/show/{slug}/{id}', 'show');
    });

    Route::post('/webhooks/midtrans', [PaymentController::class, 'webhook']);

    Route::post('/track-resi', [TrackingController::class, 'shipmentTracker']);

    Route::controller(ProductController::class)->prefix('product')->group(function () {
        Route::get('/', 'listProduct');
        Route::get('/filter', 'filterProduct');
        Route::get('/popular', 'popularProduct');
        Route::get('/{slug}', 'searchBySlugProduct');
        Route::get('/vendor/{slug}', 'listProductVendor');
    });

    Route::controller(CategoryController::class)->prefix('category')->group(function () {
        Route::get('/', 'index');
        Route::get('/{slug}', 'show');
    });

    Route::controller(VendorController::class)->prefix('/')->group(function () {
        Route::get('/{slug}', 'publicShow');
    });

    Route::controller(StatusController::class)->prefix('/status_order')->group(function () {
        Route::get('/list', 'index');
    });
});

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/logout', [AuthUserController::class, 'logout']);


    Route::prefix('/user')->group(function () {
        Route::get('/data', [UserController::class, 'show']);
        Route::get('/', [UserController::class, 'index']);
        Route::post('/update', [UserController::class, 'update']);
        Route::post('/password/update', [UserController::class, 'updatePassword']);
    });

    Route::prefix('/order')->group(function () {
        Route::post('/store/{slug}', [OrderController::class, 'store']);
    });


    Route::middleware('vendor')->group(function () {
        Route::prefix('vendor')->group(function () {
            Route::prefix('product')->group(function () {
                Route::post('/', [ProductController::class, 'store']);
                Route::get('/', [ProductController::class, 'listVendorProduct']);
                Route::post('/{slug}', [ProductController::class, 'update']);
                Route::delete('/{slug}', [ProductController::class, 'destroy']);
                Route::post('/{slug}/image/', [ProductController::class, 'deleteImage']);
                Route::get('/{slug}', [ProductController::class, 'searchBySlugVendorProduct']);
                Route::get('/image/{slug}', [ProductController::class, 'getProductImage']);
                Route::post('/update-status/{slug}', [ProductController::class, 'updatestatus']);
                Route::post('/upload-file/{slug}', [ProductController::class, 'uploadFile']);
            });
            Route::prefix('dashboard')->group(function () {
                Route::get('/name', [DashboardAdminVendorController::class, 'getName']);
                Route::get('/order/incoming', [DashboardAdminVendorController::class, 'getIncomingOrder']);
                Route::get('/order/ongoing', [DashboardAdminVendorController::class, 'getOngoingOrder']);
                Route::get('/order/finish', [DashboardAdminVendorController::class, 'getFinishOrder']);
                Route::get('/order/category', [DashboardAdminVendorController::class, 'getCategoryOrder']);
                Route::get('/order/totalOrder', [DashboardAdminVendorController::class, 'getTotalOrder']);
                Route::get('/order/totalSelling', [DashboardAdminVendorController::class, 'getTotalSelling']);
            });
            Route::get('/list', [VendorController::class, 'index']);
            Route::post('/logout', [AuthVendorController::class, 'logout']);
            Route::get('/show', [VendorController::class, 'show']);
            Route::post('/update', [VendorController::class, 'update']);
            Route::post('/update-store', [VendorController::class, 'updateStore']);
            Route::post('/password/update', [VendorController::class, 'updatePassword']);
            Route::post('/upload-file', [VendorController::class, 'uploadFile']);
            Route::post('/project/chat/store/{id}', [ProjectChatController::class, 'store']);
            Route::post('/project/chat/list/{id}', [ProjectChatController::class, 'listMessage']);
            Route::post('/project/revision/{id}', [OrderController::class, 'requestRevision']);
            Route::get('/project/list', [OrderController::class, 'getProjectList']);
            Route::get('/project/{id}', [OrderController::class, 'getProject']);
        });
    });

    // new
    Route::get('/project/info/{id}', [OrderController::class, 'projectInfo']);
    Route::get('/project/status/{id}', [OrderController::class, 'projectStatus']);
    Route::get('/project/shipment-recipt/{id}', [OrderController::class, 'shipmentRecipt']);
    Route::post('/project/accept/{id}', [OrderController::class, 'acceptProject']);
    Route::get('/project/list', [OrderController::class, 'getProjectList']);
    Route::get('/project/{id}', [OrderController::class, 'getProject']);
    // Route::get('/project/get/', [OrderController::class, 'getListOrder']);
    Route::post('/project/chat/store/{id}', [ProjectChatController::class, 'store']);
    Route::post('/project/chat/list/{id}', [ProjectChatController::class, 'listMessage']);
    Route::post('/project/revision/{id}', [OrderController::class, 'requestRevision']);

    Route::post('/shipment/send/store/{id}', [OrderController::class, 'updateOrderShipment']);
    Route::post('/shipment/recipt/store/{id}', [OrderController::class, 'storeOrderShipmentRecipt'])->middleware('vendor');
    Route::post('/shipment/show/{id}', [OrderController::class, 'getOrderShipment']);

    Route::controller(RatingController::class)->prefix('product/rating')->group(function () {
        Route::post('/store/{id}', 'store');
    });
});
