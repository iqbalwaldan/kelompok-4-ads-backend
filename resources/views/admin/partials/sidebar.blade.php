<nav id="sidebar" class="sidebar js-sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand" href="/dashboard">
            {{-- <span class="align-middle">AdminKit</span> --}}
            <img src="{{ asset('adminKit/img/brand/logo-uplift-market-white.png') }}" alt=""
                style="width: 200px">
        </a>

        <ul class="sidebar-nav">
            <li class="sidebar-header">
                Pages
            </li>

            <li class="sidebar-item {{ Request::is('dashboard*') ? 'active' : '' }}">
                <a class="sidebar-link" href="/dashboard">
                    <i class="align-middle" data-feather="sliders"></i> <span
                        class="align-middle">Dashboard</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('vendor*') ? 'active' : '' }}">
                <a class="sidebar-link" href="/vendor">
                    <i class="align-middle" data-feather="layers"></i> <span class="align-middle">Vendor</span>
                </a>
            </li>
            <li class="sidebar-item {{ Request::is('user*') ? 'active' : '' }}">
                <a class="sidebar-link" href="/user">
                    <i class="align-middle" data-feather="user"></i> <span class="align-middle">User</span>
                </a>
            </li>
        </ul>
    </div>
</nav>