@extends('admin.layouts.main')


@section('content')
    <div class="container-fluid p-0">

        <h1 class="h3 mb-3"><strong>Analytics</strong> Dashboard</h1>
        <div class="row">
            <div class="col-3">
                <div class="card flex-fill">
                    <div class="card-header">
                        <h5 class="card-title mb-0">Top 5 User</h5>
                    </div>
                    <table class="table table-hover my-0">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th class="d-none d-xl-table-cell">Spend</th>
                            </tr>
                        </thead>
                        <tbody id="topUsersTableBody">
                            <!-- Table rows will be inserted here by Ajax -->
                        </tbody>
                    </table>
                </div>
                <div class="card flex-fill">
                    <div class="card-header">
                        <h5 class="card-title mb-0">Top 5 Payment Method</h5>
                    </div>
                    <table class="table table-hover my-0">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th class="d-none d-xl-table-cell">Spend</th>
                            </tr>
                        </thead>
                        <tbody id="topUsersTableBody">
                            <!-- Table rows will be inserted here by Ajax -->
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="col-9">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col mt-0">
                                        <h5 class="card-title">Rate</h5>
                                    </div>

                                    <div class="col-auto">
                                        <div class="stat text-primary">
                                            <i class="align-middle" data-feather="star"></i>
                                        </div>
                                    </div>
                                </div>
                                <h1 class="mt-1 mb-3">{{ $rate }}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col mt-0">
                                        <h5 class="card-title">Total User</h5>
                                    </div>

                                    <div class="col-auto">
                                        <div class="stat text-primary">
                                            <i class="align-middle" data-feather="users"></i>
                                        </div>
                                    </div>
                                </div>
                                <h1 class="mt-1 mb-3">{{ $totalUser }}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col mt-0">
                                        <h5 class="card-title">Total Pemesanan</h5>
                                    </div>

                                    <div class="col-auto">
                                        <div class="stat text-primary">
                                            <i class="align-middle" data-feather="dollar-sign"></i>
                                        </div>
                                    </div>
                                </div>
                                <h1 class="mt-1 mb-3">{{ $totalPemesanan }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row d-flex flex-wrap">
                    <div class="col-lg-6 col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0">Transaksi Setiap Produk</h5>
                            </div>
                            <div class="card-body">
                                <canvas id="transactionProduct"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0">Status Pembayaran User</h5>
                            </div>
                            <div class="card-body">
                                <canvas id="paymentStatusUser"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0">Gender</h5>
                            </div>
                            <div class="card-body">
                                <canvas id="gender"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0">Payment Method</h5>
                            </div>
                            <div class="card-body">
                                <canvas id="paymentMethod"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="col-3">
            <div class="row">
                <div class="col-12 col-lg-2 col-xxl-3 d-flex">
                    <div class="card flex-fill">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Top 5 User</h5>
                        </div>
                        <table class="table table-hover my-0">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th class="d-none d-xl-table-cell">Spend</th>
                                </tr>
                            </thead>
                            <tbody id="topUsersTableBody">
                                <!-- Table rows will be inserted here by Ajax -->
                            </tbody>
                        </table>
                    </div>
            </div>
        </div> --}}
    @endsection

    @push('scripts')
        <script type="text/javascript">
            $(document).ready(function() {
                $.ajax({
                    url: '/top-users',
                    type: 'GET',
                    dataType: 'json',
                    success: function(response) {

                        $('#topUsersTableBody').empty();

                        $.each(response.topUsers, function(index, user) {
                            var row = '<tr>' +
                                '<td>' + user.first_name + '</td>' +
                                '<td class="d-none d-xl-table-cell">' + formatCurrency(user
                                    .total_amount) +
                                '</td>' +
                                '</tr>';
                            $('#topUsersTableBody').append(row);
                        });
                    },
                    error: function(error) {
                        console.error('Error fetching top users data:', error);
                    },
                });

                function formatCurrency(amount) {
                    const formattedAmount = new Intl.NumberFormat('id-ID', {
                        style: 'currency',
                        currency: 'IDR',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2
                    }).format(amount);

                    return formattedAmount;
                }

                // Transaction Product bar chart
                $.ajax({
                    url: '/getTransactionProduct', // Ganti dengan URL sesuai dengan rute Anda
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        // Mengambil data dari response JSON
                        const labelsTransactionProduct = data.map(item => item.name);
                        const dataTransactionProduct = {
                            labels: labelsTransactionProduct,
                            datasets: [{
                                label: 'Transaction Product',
                                data: data.map(item => item.subtotal),
                                backgroundColor: [
                                    '#0E493A',
                                    '#1C9275',
                                    '#68B6A3',
                                ],
                            }]
                        };

                        const configTransactionProduct = {
                            type: 'bar',
                            data: dataTransactionProduct,
                            options: {
                                scales: {
                                    y: {
                                        beginAtZero: true
                                    }
                                }
                            },
                        };

                        const transactionBarChart = new Chart($('#transactionProduct'),
                            configTransactionProduct);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });

                // const labelsTransactionProduct = ['Fotografi', 'Desain', 'Videografi']

                // const dataTransactionProduct = {
                //     labels: labelsTransactionProduct,
                //     datasets: [{
                //         label: 'Transaction Product',
                //         data: [90, 59, 80],
                //         backgroundColor: [
                //             '#0E493A',
                //             '#1C9275',
                //             '#68B6A3',
                //         ],

                //     }, ]
                // };

                // const configTransactionProduct = {
                //     type: 'bar',
                //     data: dataTransactionProduct,
                //     options: {
                //         scales: {
                //             y: {
                //                 beginAtZero: true
                //             }
                //         }
                //     },
                // };

                // const transactionBarChart = new Chart($('#transactionProduct'), configTransactionProduct)

                // Payment Status User bar chart
                const labelsPaymentStatusUser = ['Fotografi', 'Desain', 'Videografi']

                const dataPaymentStatusUser = {
                    labels: labelsPaymentStatusUser,
                    datasets: [{
                            label: 'Belum Selesai',
                            data: [34, 54, 65],
                            backgroundColor: [
                                '#0E493A',
                                '#0E493A',
                                '#0E493A',
                            ],
                        },
                        {
                            label: 'Revisi',
                            data: [43, 75, 64],
                            backgroundColor: [
                                '#1C9275',
                                '#1C9275',
                                '#1C9275',
                            ],
                        },
                        {
                            label: 'Sudah Selesai',
                            data: [78, 35, 89],
                            backgroundColor: [
                                '#68B6A3',
                                '#68B6A3',
                                '#68B6A3',
                            ],
                        },
                    ]
                };
                const configPaymentStatusUser = {
                    type: 'bar',
                    data: dataPaymentStatusUser,
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    },
                };

                const paymentStatusBarChart = new Chart($('#paymentStatusUser'), configPaymentStatusUser)

                // Gender pie chart
                const genderData = {
                    labels: [
                        'Male',
                        'Female',

                    ],
                    datasets: [{
                        label: 'Gender',
                        data: [50, 90],
                        backgroundColor: [
                            '#0E493A',
                            '#1C9275',

                        ],
                    }]

                };

                const configGender = {
                    type: 'pie',
                    data: genderData,
                };

                const genderPieChart = new Chart($('#gender'), configGender)

                // Payment doughnut chart
                function generateRandomColor() {
                    var color = 'rgb(' +
                        Math.floor(Math.random() * 256) + ',' +
                        Math.floor(Math.random() * 256) + ',' +
                        Math.floor(Math.random() * 256) + ')';
                    return color;
                }

                const paymentMethodData = {
                    labels: [
                        'BNI',
                        'BRI',
                        'Mandiri',
                        'BTN',
                        'BSI',

                    ],
                    datasets: [{
                        label: 'Payment Method',
                        data: [50, 90, 12, 39, 53],
                        backgroundColor: [1, 2, 3, 4, 5].map(() => generateRandomColor())
                    }]

                };

                const configPaymentMethod = {
                    type: 'doughnut',
                    data: paymentMethodData,
                };

                const paymentMethodChart = new Chart($('#paymentMethod'), configPaymentMethod)
            });
        </script>
    @endpush
