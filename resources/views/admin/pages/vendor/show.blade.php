@extends('admin.layouts.main')

@section('content')
    <div class="card">
        <div class="card-body">
            <h3 class="fw-bold">Informasi Vendor</h3>
            <h5 class="fw-bold">Owner name</h5>
            <p>{{ $vendor->name }}</p>
            <h5 class="fw-bold">Birth</h5>
            <p>{{ $vendor->dob }}</p>
            <h5 class="fw-bold">NIK</h5>
            <p>{{ $vendor->nik }}</p>
            <h5 class="fw-bold">KTP</h5>
            @foreach ($vendor->getMedia('vendor-ktp') as $ktp)
                <img src="{{ $ktp->getUrl() }}" width="400" class="me-4" alt="Bukti Laporan">
            @endforeach
            <br><br>
            <h5 class="fw-bold">Email</h5>
            <p>{{ $vendor->email }}</p>
            <h5 class="fw-bold">Phone Number</h5>
            <p>{{ $vendor->phone_number }}</p>
            <h5 class="fw-bold">Business Name</h5>
            <p>{{ $vendor->business_name }}</p>
            <h5 class="fw-bold">Business Logo</h5>
            @foreach ($vendor->getMedia('vendor-logo') as $logo)
                <img src="{{ $logo->getUrl() }}" width="400" class="me-4" alt="Bukti Laporan">
            @endforeach
            <br><br>
            <h5 class="fw-bold">Email Business</h5>
            <p>{{ $vendor->email_business }}</p>
            <h5 class="fw-bold">Phone Number Business</h5>
            <p>{{ $vendor->phone_business }}</p>
            <h5 class="fw-bold">Address Business</h5>
            <p>{{ $vendor->address }}, {{ $vendor->province }}, {{ $vendor->city }}, {{ $vendor->postal_code }}</p>
            <h5 class="fw-bold">Portofolio</h5>
            @foreach ($vendor->getMedia('vendor-portofolio') as $portofolio)
                <iframe src="{{ $portofolio->getUrl() }}" frameborder="0" width="1300px" height="500px"></iframe>
            @endforeach
            <br><br>
            <h5 class="fw-bold">Status</h5>
            <form action="/vendor/update/{{ $vendor->id }}" method="POST">
                @csrf
                <select class="form-select mb-3" name="status">
                    <option value="process" {{ $vendor->status === 'process' ? 'selected' : '' }}>Proses</option>
                    <option value="verified" {{ $vendor->status === 'verified' ? 'selected' : '' }}>Diverifikasi</option>
                </select>
                <a href="/vendor" class="btn btn-secondary">Cancel</a>
                <button type="submit" class="btn btn-primary custom">Update</button>
            </form>
        </div>
    </div>
@endsection
