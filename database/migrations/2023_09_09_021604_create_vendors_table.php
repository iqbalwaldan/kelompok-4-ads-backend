<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->date('dob');
            $table->string('nik')->uniqid();
            $table->string('email')->uniqid();
            $table->string('phone_number');
            $table->string('password');
            $table->string('business_name');
            $table->string('slug');
            $table->string('description')->nullable();
            $table->string('email_business')->uniqid();
            $table->string('phone_business');
            $table->string('address');
            $table->string('province');
            $table->string('city');
            $table->string('postal_code');
            $table->string('status');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('know');
            $table->string('reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vendors');
    }
};
