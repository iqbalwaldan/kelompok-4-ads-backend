<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->constrained('orders');
            $table->foreignId('product_id')->constrained('products');
            // $table->foreignId('order_status_id')->constrained();
            $table->integer('quantity');
            $table->double('price');
            $table->double('admin_fee');
            $table->double('subtotal');
            $table->string('title');
            $table->text('brief');
            $table->string('link');
            $table->string('reason_reject')->nullable();
            $table->integer('order_status_id')->default(1);
            $table->string('shipment')->nullable();
            $table->string('no_resi')->nullable();
            $table->integer('revision')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('order_details');
    }
};
