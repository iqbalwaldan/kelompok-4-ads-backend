<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'first_name' => 'Iqbal',
                'last_name' => 'Waldan',
                'email' => 'iqbal@gmail.com',
                'password' => bcrypt('password'),
                'phone_number' => '081234567890',
                'gender' => 'male',
                'isAdmin' => true,
            ],
            [
                'first_name' => 'Amanda',
                'last_name' => 'Smith',
                'email' => 'amanda@example.com',
                'password' => bcrypt('password'),
                'phone_number' => '082345678901',
                'gender' => 'female',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Rizki',
                'last_name' => 'Pratama',
                'email' => 'rizkii@hotmail.com',
                'password' => bcrypt('password'),
                'phone_number' => '083456789012',
                'gender' => 'male',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Siti',
                'last_name' => 'Wulandari',
                'email' => 'sitii@yahoo.com',
                'password' => bcrypt('password'),
                'phone_number' => '084567890123',
                'gender' => 'female',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Hadi',
                'last_name' => 'Santoso',
                'email' => 'hadii@gmail.com',
                'password' => bcrypt('password'),
                'phone_number' => '085678901234',
                'gender' => 'male',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Linda',
                'last_name' => 'Williams',
                'email' => 'lindaa@example.com',
                'password' => bcrypt('password'),
                'phone_number' => '086789012345',
                'gender' => 'female',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Budi',
                'last_name' => 'Kurniawan',
                'email' => 'budii@hotmail.com',
                'password' => bcrypt('password'),
                'phone_number' => '087890123456',
                'gender' => 'male',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Anisa',
                'last_name' => 'Rahmawati',
                'email' => 'anisaa@yahoo.com',
                'password' => bcrypt('password'),
                'phone_number' => '088901234567',
                'gender' => 'female',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Ahmad',
                'last_name' => 'Hermawan',
                'email' => 'ahmadd@gmail.com',
                'password' => bcrypt('password'),
                'phone_number' => '089012345678',
                'gender' => 'male',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Sari',
                'last_name' => 'Lestari',
                'email' => 'sarii@example.com',
                'password' => bcrypt('password'),
                'phone_number' => '089123456789',
                'gender' => 'female',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Nurul',
                'last_name' => 'Aini',
                'email' => 'nurul@gmail.com',
                'password' => bcrypt('password'),
                'phone_number' => '081234567890',
                'gender' => 'male',
                'isAdmin' => true,
            ],
            [
                'first_name' => 'Amanda',
                'last_name' => 'Smith',
                'email' => 'amandaa@example.com',
                'password' => bcrypt('password'),
                'phone_number' => '082345678901',
                'gender' => 'female',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Rizki',
                'last_name' => 'Pratama',
                'email' => 'rizki@hotmail.com',
                'password' => bcrypt('password'),
                'phone_number' => '083456789012',
                'gender' => 'male',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Siti',
                'last_name' => 'Wulandari',
                'email' => 'siti@yahoo.com',
                'password' => bcrypt('password'),
                'phone_number' => '084567890123',
                'gender' => 'female',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Hadi',
                'last_name' => 'Santoso',
                'email' => 'hadi@gmail.com',
                'password' => bcrypt('password'),
                'phone_number' => '085678901234',
                'gender' => 'male',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Linda',
                'last_name' => 'Williams',
                'email' => 'linda@example.com',
                'password' => bcrypt('password'),
                'phone_number' => '086789012345',
                'gender' => 'female',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Budi',
                'last_name' => 'Kurniawan',
                'email' => 'budi@hotmail.com',
                'password' => bcrypt('password'),
                'phone_number' => '087890123456',
                'gender' => 'male',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Anisa',
                'last_name' => 'Rahmawati',
                'email' => 'anisa@yahoo.com',
                'password' => bcrypt('password'),
                'phone_number' => '088901234567',
                'gender' => 'female',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Ahmad',
                'last_name' => 'Hermawan',
                'email' => 'ahmad@gmail.com',
                'password' => bcrypt('password'),
                'phone_number' => '089012345678',
                'gender' => 'male',
                'isAdmin' => false,
            ],
            [
                'first_name' => 'Sari',
                'last_name' => 'Lestari',
                'email' => 'sari@example.com',
                'password' => bcrypt('password'),
                'phone_number' => '089123456789',
                'gender' => 'female',
                'isAdmin' => false,
            ],
        ];

        foreach ($data as $user) {
            User::create($user);
        }
    }
}
