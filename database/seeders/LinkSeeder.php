<?php

namespace Database\Seeders;

use App\Models\Link;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'vendor_id' => 1,
                'name' => 'Google',
                'url' => 'https://www.google.com/',
            ],
            [
                'vendor_id' => 2,
                'name' => 'Facebook',
                'url' => 'https://www.facebook.com/',
            ],
            [
                'vendor_id' => 3,
                'name' => 'Amazon',
                'url' => 'https://www.amazon.com/',
            ],
            [
                'vendor_id' => 4,
                'name' => 'Microsoft',
                'url' => 'https://www.microsoft.com/',
            ],
            [
                'vendor_id' => 5,
                'name' => 'Apple',
                'url' => 'https://www.apple.com/',
            ],
            [
                'vendor_id' => 6,
                'name' => 'Twitter',
                'url' => 'https://www.twitter.com/',
            ],
            [
                'vendor_id' => 7,
                'name' => 'LinkedIn',
                'url' => 'https://www.linkedin.com/',
            ],
            [
                'vendor_id' => 8,
                'name' => 'Netflix',
                'url' => 'https://www.netflix.com/',
            ],
            [
                'vendor_id' => 9,
                'name' => 'Instagram',
                'url' => 'https://www.instagram.com/',
            ],
            [
                'vendor_id' => 10,
                'name' => 'YouTube',
                'url' => 'https://www.youtube.com/',
            ]
        ];

        foreach ($data as $link) {
            Link::create($link);
        }
    }
}
