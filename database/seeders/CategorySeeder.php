<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'name' => 'photography',
            ],
            [
                'name' => 'videography',
            ],
            [
                'name' => 'design',
            ]
        ];
        
        foreach ($data as $category) {
            Category::create($category);
        }
    }
}
